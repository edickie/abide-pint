
# coding: utf-8

# In[10]:

import nibabel as nib
import numpy as np


# In[11]:

## after the last fslness we have swaped the dimensions but there is still a problem with the header
badorientation_nii = 'mprage_badorientation.nii'
goodorienttion = 'mprage_tmp.nii.gz'


# In[12]:

## load both images 
bad_nib = nib.load(badorientation_nii)
good_nib = nib.load(goodorienttion)
## load only the data from the good_nib
good_data = good_nib.get_data()




# In[14]:

## add the header of the old image back to the good data header
new1 = nib.Nifti1Image(good_data, bad_nib.affine)
nib.save(new1, "mprage.nii.gz")

