#!/bin/bash

#load the epitome enviroment
module load gnu-parallel/20140622
source ${HOME}/myscripts/abideII/epitome_env_nodatapaths.sh

PintOutputs=/scratch/a/arisvoin/edickie/ABIDE/20160914_PINT_fixa_Yeo7-80v_rad6-6-12/
ConcatenatedOutput=${PintOutputs}/20160914_PINT_fixa_Yeo7-80v_rad6-6-12_concatenatedresults.csv
OutputDir=/scratch/a/arisvoin/edickie/ABIDE/20160914_PINT_fixa_Yeo7-80v_rad6-6-12-sub2sub-mm 
surfL=/scratch/a/arisvoin/edickie/HCP_S900_GroupAvg_v1/S900.L.midthickness_MSMAll.32k_fs_LR.surf.gii
surfR=/scratch/a/arisvoin/edickie/HCP_S900_GroupAvg_v1/S900.R.midthickness_MSMAll.32k_fs_LR.surf.gii

cd $PintOutputs
SUBJECTS=`ls -1d *_5*`

parallel "echo python ${HOME}/myscripts/abide/PINT/calc_sub2sub_20160912.py {} $ConcatenatedOutput $OutputDir $surfL $surfR" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --walltime 01:30:00 -c 45 -j 4 --ppj 8 -N sub2sub${SITE} -
