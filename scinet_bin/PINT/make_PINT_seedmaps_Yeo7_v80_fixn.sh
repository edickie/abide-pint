#!/bin/bash

## set the site variable here
SITE=${1}
SUBID=${2}
SamplingRad=${3}
PINToutputs=${4}
OutName=${5}

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/ciftify_env_nodatapaths.sh
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

export TMPDIR=${tmpdir}

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${TMPDIR} on "
    date
    rm -rf ${TMPDIR}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}

## now for the RSN-viewer
SeedMaps=${PINToutputs}_seedmaps
mkdir -p ${SeedMaps}/${SUBID}

echo $SUBID

python ${HOME}/myscripts/abide/PINT/make_subject_seedmaps.py \
  --sampling-radius ${SamplingRad}\
  ${HCP_DATA}/${SUBID}/MNINonLinear/Results/rest_abide25fix/rest_abide25fix_Atlas_s8.dtseries.nii \
  ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.L.midthickness.32k_fs_LR.surf.gii \
  ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.R.midthickness.32k_fs_LR.surf.gii \
  ${PINToutputs}/${SUBID}/${SUBID}_${OutName}_summary.csv \
  ${SeedMaps}/${SUBID}/${SUBID}
