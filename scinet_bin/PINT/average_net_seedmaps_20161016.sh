
source ~/myscripts/abide/ciftify_env_nodatapaths.sh

cd ${SCRATCH}/ABIDE/20161007_PINT_fixn_Yeo7-80v_rad6-6-12_seedmaps

#mkdir average_allQCed
subjects=`cat ~/myscripts/abide/randomise/ABIDEI_glm889_DX_subids.txt`
nsubs=889

mkdir ABIDEI_glm889_inputs

for vertex_type in ivertex tvertex; do
  for network in 2 3 4 5 6 7; do
    ciftilist=""
    for subject in $subjects; do
       ciftilist="$ciftilist -cifti ${subject}/${subject}_${vertex_type}_net${network}.dscalar.nii"
    done
    wb_command -cifti-merge ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}.dscalar.nii $ciftilist
   done
done
