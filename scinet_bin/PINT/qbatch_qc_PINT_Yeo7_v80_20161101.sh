#!/bin/bash

## set the site variable here
SITES="CALTECH
CMU
KKI
LEUVEN_1
LEUVEN_2
MAX_MUN
NYU
OLIN
PITT
SBL
SDSU
TRINITY
UCLA_1
UCLA_2
UM_1
UM_2
USM
YALE"


#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh

## run the rad4-8-12
OutBase=${SCRATCH}/ABIDE/20161101_PINT

## figure out the qc directory name and make the stuffs...
SamplingRad=6
SearchRad=6
PaddingRad=12
OutName="Yeo7-80v_rad${SamplingRad}-${SearchRad}-${PaddingRad}"
PINToutputs=${OutBase}_${OutName}
base_PINTout=$(basename ${PINToutputs})
qcdir="qc_${base_PINTout}"

mkdir ${SCRATCH}/ABIDE/${qcdir}

## now submit the job
cd ${PINToutputs}

for SITE in $SITES; do
  SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
  parallel "echo ${HOME}/myscripts/abide/PINT/qc_PINT_Yeo7_v80_ciftify.sh ${SITE} {} ${OutBase}" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 04:00:00 -c 20 -j 4 --ppj 8 -N pintqc${SITE} -
done
