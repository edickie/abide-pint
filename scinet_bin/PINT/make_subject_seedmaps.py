#!/usr/bin/env python
"""
Will measure the distance from one subjects rois to all other subjects in mm on specified surfaces.

Usage:
  make_subject_seedmaps.py [options] <func.dtseries.nii> <left-surface.gii> <right-surface.gii> <pint-summary> <outputprefix>

Arguments:
    <func.dtseries.nii>    Paths to directory source image
    <left-surface.gii>     Path to template for the ROIs of network regions
    <right-surface.gii>    Surface file .surf.gii to read coordinates from
    <pint-summary>         Table of template vertices from which to Start
    <outputprefix>         Output directory for seedmap outputs

Options:
  --sampling-radius MM       Radius [default: 6] in mm of sampling rois
  --debug                    Debug logging in Erin's very verbose style
  -n,--dry-run               Dry run
  --help                     Print help

DETAILS
Requires that all PINT summary files have already been calculated by PINT
Usummed you are using the Yeo_v80 template for PINT.

Written by Erin W Dickie, Oct 14, 2016
"""


import os
import sys
import tempfile
import shutil
import subprocess
from docopt import docopt

## function for doing stuffs in the shell
def docmd(cmdlist):
    "sends a command (inputed as a list) to the shell"
    if DEBUG: print ' '.join(cmdlist)
    if not DRYRUN: subprocess.call(cmdlist)

def main():
    global DEBUG
    global DRYRUN

    arguments = docopt(__doc__)
    func          = arguments['<func.dtseries.nii>']
    surfL         = arguments['<left-surface.gii>']
    surfR         = arguments['<right-surface.gii>']
    summary_csv = arguments['<pint-summary>']
    output_prefix = arguments['<outputprefix>']
    sampling_rad = arguments['--sampling-radius']
    DEBUG = arguments['--debug']
    DRYRUN = arguments['--dry-run']

    if DEBUG: print(arguments)

    ## make the tempdir
    tmpdir = tempfile.mkdtemp()

    outputbase  = os.path.basename(output_prefix)


    ## output the correlation mask
    func_mask = '{}_mask.dscalar.nii'.format(output_prefix)
    docmd(['wb_command',
           '-cifti-math',
           '(x > 100)',
           func_mask,
           '-var', 'x', func, '-select', '1', '5'])

    for vertex_type in ['tvertex','ivertex']:

        ## make the circular rois
        xrois_dscalar = os.path.join(tmpdir, '{}_{}_rois.dscalar.nii'.format(outputbase, vertex_type))
        docmd(['ciftify-surface-rois',
            '--vertex-col', vertex_type,
            '--labels-col', 'NETWORK',
            summary_csv,
            str(sampling_rad),
            surfL,
            surfR,
            xrois_dscalar])

        for network in [2,3,4,5,6,7]:
            out_seeddscalar = os.path.join('{}_{}_net{}.dscalar.nii'.format(output_prefix,vertex_type, network))
            tmpd_base = os.path.join(tmpdir,'{}_net{}c'.format(vertex_type,network))
            # calculate the seed map
            docmd(['ciftify-seed-corr',
                   '--surfaceonly',
                   '--roi-label', '{}'.format(network),
                   '--outputname', tmpd_base,
                   '--mask', func_mask,
                    func,
                    xrois_dscalar])
            # convert the seedcorr map to Z scores
            docmd(['wb_command', '-cifti-math', 'atanh(x)',
                   out_seeddscalar,
                   '-var', 'x', '{}.dscalar.nii'.format(tmpd_base)])

    #get rid of the tmpdir
    shutil.rmtree(tmpdir)

if __name__ == "__main__":
    main()
