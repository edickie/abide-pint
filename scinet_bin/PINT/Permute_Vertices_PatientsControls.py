#!/usr/bin/env python

"""
Given two groups (e.g. patients and controls), this script permutes groups
for each of 80 ROIs to identify the vertices in each ROI that are significantly
more likely to have subjects from one group vs. the other.

Usage:
    Permute_Vertices_PatientsControls.py [options] <inputcsv> <groupingvariable> <outputfolder>

Arguments:
    <inputcsv>              Path to a folder that contains the input csv.
    <groupingvariable>      The column header for the column containing groups 1 and 2.
    <outputfolder>          Folder where the output should be stored.

Options:
    --permute-only             Only run the permutations
    --summarise-only           Only run the final summarise
    --concat-permutations TXT  Input a text file of merged-file paths to concatenate
    --n-permutations N         The number [default: 10] of permutations to run
    --radius MM                Radius in mm [default: 6] of ROIs
    --alpha THRESHOLD          Significance threshold [default: 0.05] two-tailed
    --vertex-col colname       Column name [default: vertex] containing the vertex ids
    --debug                    Debug logging
    -v,--verbose               Verbose logging
    -n,--dry-run               Dry run
    -h,--help                  Prints this message

Details:
The code assigns values of 0, 1, or 2 to every vertex within an ROI,
where 0 = This vertex is not significantly associated with subjects from either group,
1 = This vertex is significantly more likely to have subjects from
Group 1, 2 = This vertex is significantly more likely to have subjects from Group 2.

The input csv files should contain a vertex list, hemisphere, group label,
and could have other optional labels.

Output: If input is a folder, one cifti dscalar.nii file
for each ROI and one merged cifti dscalar file containing output for all ROIs.
If input is a file, one cifti dscalar.nii file for the specified ROI.

Written by Saba Shahab, February 28, 2017

"""
import os
import sys
import subprocess
import numpy as np
import scipy as sp
import nibabel as nib
import pandas as pd
import tempfile
import shutil
import nibabel.gifti.giftiio
import ciftify
from docopt import docopt
import logging
import sklearn.utils

logger = logging.getLogger(os.path.basename(__file__))
log_handler = logging.StreamHandler(sys.stdout)
log_handler.setFormatter(logging.Formatter('[%(name)s] %(levelname)s : '
        '%(message)s'))
logger.addHandler(log_handler)
logger.setLevel(logging.DEBUG)

def file_content(fname):
	# Returns the contents of a file as a list, with each line as a separate list item.
	with open(fname) as f:
		content = f.readlines()
		content = [x.strip() for x in content]
		return content

def run(cmd, dryrun=False, echo=True, supress_stdout = False):
    """
    Runs command in default shell, returning the return code. And logging the output.
    It can take a the cmd argument as a string or a list.
    If a list is given, it is joined into a string.
    There are some arguments for changing the way the cmd is run:
       dryrun:     do not actually run the command (for testing) (default: False)
       echo:       Print the command to the log (info (level))
       supress_stdout:  Any standard output from the function is printed to the log at "debug" level but not "info"
    """

    global DRYRUN
    dryrun = DRYRUN

    if type(cmd) is list:
        thiscmd = ' '.join(cmd)
    else: thiscmd = cmd
    if echo:
        logger.info("Running: {}".format(thiscmd))
    if dryrun:
        logger.info('Doing a dryrun')
        return 0
    else:
        p = subprocess.Popen(thiscmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if p.returncode:
            logger.error('cmd: {} \n Failed with returncode {}'.format(thiscmd, p.returncode))
        if supress_stdout:
            logger.debug(out)
        else:
            logger.info(out)
        if len(err) > 0 : logger.warning(err)
        return out

def run_geodesic_rois(groupdf, roi_radius, output_dsclar, tmpdir):
		'''
		write groupdf to csv and run ciftify-surface-rois with probmap option
		'''

		## send groupdf to csv
		group_file = os.path.join(tmpdir,'Group.csv')
		groupdf.to_csv(group_file)

		## use defaults to define the surface
		surfL = os.path.join(
			ciftify.config.find_HCP_S900_GroupAvg(),
			'S900.L.midthickness_MSMAll.32k_fs_LR.surf.gii')
		surfR = os.path.join(
			ciftify.config.find_HCP_S900_GroupAvg(),
			'S900.R.midthickness_MSMAll.32k_fs_LR.surf.gii')

		## run ciftify-surface-rois
		run("ciftify-surface-rois --probmap {} {} {} {} {}".format(group_file,
		 														roi_radius,
																surfL, surfR,
																output_dsclar))

def calc_group_difference(inputdf, groupingvariable, roi_radius, output_difference_dscalar, tmpdir, groupfile_dir = None):
	'''
	makes probmap for both groups and calculates the difference
	'''
	## set groupfile_dir to tmpdir if not defined
	if groupfile_dir == None: groupfile_dir = tmpdir
	## run group1 probmap
	for group in [1,2]:
		run_geodesic_rois(inputdf[inputdf[groupingvariable] == group],
				roi_radius,
				os.path.join(groupfile_dir,'Group{}.dscalar.nii'.format(group)),
				tmpdir)

	## calc the difference
	run("wb_command -cifti-math \"(x-y)\" {} -var x {} -var y {}".format(
		output_difference_dscalar,
		os.path.join(groupfile_dir,'Group1.dscalar.nii'),
		os.path.join(groupfile_dir,'Group2.dscalar.nii')))

def main(arguments, tmpdir):

	print(arguments)

	permute_only = arguments['--permute-only']
	summarise_only = arguments['--summarise-only']
	inputcsv = arguments['<inputcsv>']
	groupingvariable = arguments['<groupingvariable>']
	outputfolder = arguments['<outputfolder>']
	numberofpermutations = arguments['--n-permutations']
	merged_list_file = arguments['--concat-permutations']
	roi_radius = arguments['--radius']
	alpha_threshold = arguments['--alpha']

	logger.info(arguments)

	surfL = os.path.join(
		ciftify.config.find_HCP_S900_GroupAvg(),
		'S900.L.midthickness_MSMAll.32k_fs_LR.surf.gii')
	surfR = os.path.join(
		ciftify.config.find_HCP_S900_GroupAvg(),
		'S900.R.midthickness_MSMAll.32k_fs_LR.surf.gii')

	# CSVFileNames contains the names of all CSV files where each CSV file contains information for 1 out of the 80 ROIs.
	# CSVFileNames = file_content(filenamelist)
	FileName = inputcsv

	# Creating subfolders for each subject within the main output folder.
	FilenameWithoutExtension = os.path.splitext(os.path.basename(FileName))[0]
	logger.info("Inputfile: {}".format(FilenameWithoutExtension))
	outputfolder = os.path.join(outputfolder,FilenameWithoutExtension)
	run("mkdir -p {}".format(outputfolder))
	logger.info("Outputfolder: {}".format(outputfolder))

	InputFile = FileName
	# Check if file exists
	if not os.path.isfile(InputFile):
		sys.exit("Input file: {} not found.".format(InputFile))

	## read in the input csv file
	df = pd.read_csv(InputFile)
	# Check if grouping variable exists as column name in file.
	if groupingvariable not in df.keys():
		sys.exit("Grouping variable name {} Not found in file: {}".format(groupingvariable,InputFile))
		# Divide the actual dataframe into group1 and group2

	if not summarise_only:

		OutputDifference_file = os.path.join(tmpdir,'GroupDifference.dscalar.nii')
		OutputMerged_file = os.path.join(outputfolder, 'Merged.dscalar.nii')

		# Doing the permutations
		for i in range(int(numberofpermutations)):
			logger.info("permutation {}.format(i)")

			# Create a copy of the dataframe for shuffling.
			shuffled_df = df.copy()

			# Shuffle the groups
			shuffled_df[groupingvariable] = np.random.permutation(df[groupingvariable])

			calc_group_difference(shuffled_df, groupingvariable, roi_radius, OutputDifference_file, tmpdir)

			if i == 0:
				calc_group_difference(shuffled_df, groupingvariable, roi_radius, OutputMerged_file, tmpdir)

			else:
				calc_group_difference(shuffled_df, groupingvariable, roi_radius, OutputDifference_file, tmpdir)
				# Here I would like to merge all the permutations and the actual and then after the loop, get the percentiles.
				run("wb_command -cifti-merge {} -cifti {} -cifti {}".format(OutputMerged_file, OutputMerged_file, OutputDifference_file))

	if not permute_only:

		if merged_list_file:
			merged_list = file_content(merged_list_file)
			if len(merged_list) == 1:
				merged_for_summary = merged_list[0]
			elif len(merged_list) > 1:
				# merge the merge list files into one file
				merged_for_summary = os.path.join(outputfolder, 'MergeAllPermutations.dscalar.nii')
				mergecmd = "wb_command -cifti-merge {}".format(merged_for_summary)
				for cfile in merged_list:
					mergecmd = "{} -cifti {}".format(mergecmd, cfile)
				run(mergecmd)
		else:
			merged_for_summary = OutputMerged_file

		## calculate the actual difference
		Actual_Difference = os.path.join(outputfolder, 'ActualDifference.dscalar.nii')
		calc_group_difference(df, groupingvariable, roi_radius, Actual_Difference, tmpdir, groupfile_dir = outputfolder)

		# merge the actual difference with the random permutations
		run("wb_command -cifti-merge {} -cifti {} -cifti {}".format(merged_for_summary, merged_for_summary, Actual_Difference))

		# Transpose the final matrix as -cifti-stats does stats along columns
		TransposedOutputMerged_file = os.path.join(tmpdir, 'TransposedMerged.dscalar.nii')
		run("wb_command -cifti-transpose {} {}".format(merged_for_summary, TransposedOutputMerged_file))

		## calculated the percentile cut off from the alpha
		lower_thres = float(alpha_threshold) * 100 /2 ## note divide by two to get two-tailed
		upper_thres = 100 - lower_thres

		for percentile, tail, greaterlessthan in [(lower_thres,'lower', '<'), (upper_thres,'upper','>')]:

			#Naming variables - if below is tested, this section needs to move up - to the variable initiation section.
			percentile_text_file = os.path.join(tmpdir,'{}_percentile.txt'.format(tail))
			percentile_dscalar = os.path.join(outputfolder, 'percentile.{}.dscalar.nii'.format(percentile))
			Significant_dscalar = os.path.join(outputfolder,'significant.{}.dscalar.nii'.format(percentile))
			# Where are these values being stored? This has not been tested! Store these in a text file.
			percentile_vector = run("wb_command -cifti-stats {} -percentile {}".format(TransposedOutputMerged_file, percentile))
			## write the vector to a text file
			with open(percentile_text_file, "w") as text_file:
				text_file.write("%s" % percentile_vector)
			# Creating cifti files from the text files
			run("wb_command -cifti-convert -from-text {} {} {}".format(percentile_text_file, Actual_Difference, percentile_dscalar))
			# Five_percentile (To view both together, overlay them)
			run("wb_command -cifti-math \"(z {} x)\" {} -var x {} -var z {}".format(greaterlessthan, Significant_dscalar, percentile_dscalar, Actual_Difference))


if __name__=='__main__':

    arguments  = docopt(__doc__)

    global DRYRUN

    VERBOSE = arguments['--verbose']
    DEBUG = arguments['--debug']
    DRYRUN = arguments['--dry-run']

    # create a local tmpdir
    tmpdir = tempfile.mkdtemp()
    logger.setLevel(logging.WARNING)

    if VERBOSE:
        logger.setLevel(logging.INFO)

    if DEBUG:
        logger.setLevel(logging.DEBUG)

    # formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # logger.setFormatter(formatter)

    logger.info('Creating tempdir:{} on host:{}'.format(tmpdir, os.uname()[1]))
    ret = main(arguments, tmpdir)
    shutil.rmtree(tmpdir)
    sys.exit(ret)
