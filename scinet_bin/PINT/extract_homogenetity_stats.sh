#!/bin/bash

# script that runs for every subject to calculate homogenetity related stats for every subject after running PINT
# also requires that pint seed correlation maps have been generated..

SUBID=${1}               # subject identifier
HCP_DATA=${2}            # the HCP_DATA_dir
PINT_outputdir=${3}      # the base directory for the PINT outputs
seed_maps_dir=${4}       # the base directory for the seed maps
PINT_outname=${5}        # the string in the PINT output names
PINT_sampling_rad=${5}   # the sampling radius
mem_limit=${7}           # memory limit option for the gradient map creation
outdir=${8}              # base output directory to hold the outputs

#############################

### create a tempdir in dev sham that will delete when the job ends
tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

########################

mkdir ${outdir}/${SUBID}

wb_command -cifti-correlation-gradient \
 ${HCP_DATA}/${SUBID}/MNINonLinear/Results/rest_abide25fix/rest_abide25fix_Atlas_s8.dtseries.nii \
 ${tmpdir}/${SUBID}_gradients.dscalar.nii \
 -left-surface ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.L.midthickness.32k_fs_LR.surf.gii \
 -right-surface ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.R.midthickness.32k_fs_LR.surf.gii \
 -fisher-z -surface-exclude 12 -mem-limit ${mem_limit}

for vertex_type in ivertex tvertex; do
  ciftify_surface_rois --vertex-col ${vertex_type} \
    --labels-col "roiidx" \
    ${PINT_outputdir}/${SUBID}/${SUBID}_${PINT_outname}_summary.csv \
    6 \
    ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.L.midthickness.32k_fs_LR.surf.gii \
    ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.R.midthickness.32k_fs_LR.surf.gii \
    ${tmpdir}/${SUBID}_${vertex_type}_rois.dscalar.nii

  wb_command -cifti-label-import \
    ${tmpdir}/${SUBID}_${vertex_type}_rois.dscalar.nii \
    ${CIFTIFY_TEMPLATES}/PINT/Yeo7_2011_80verts_roiidx_LUT.txt \
    ${outdir}/${SUBID}/${SUBID}_${vertex_type}.dlabel.nii

    ## extract mean gradient value for each..
    wb_command -cifti-parcellate \
      ${tmpdir}/${SUBID}_gradients.dscalar.nii \
      ${outdir}/${SUBID}/${SUBID}_${vertex_type}.dlabel.nii \
      COLUMN \
      ${tmpdir}/${SUBID}_${vertex_type}_gradients_MEAN.pscalar.nii

    wb_command -cifti-convert -to-text \
      ${tmpdir}/${SUBID}_${vertex_type}_gradients_MEAN.pscalar.nii \
      ${outdir}/${SUBID}/${SUBID}_${vertex_type}_gradients_MEAN.tsv

    ## extract TSNR and STDEV within each roi from each seed map
    for network in 2 3 4 5 6 7; do
      for stat in STDEV TSNR; do
      wb_command -cifti-parcellate \
        ${seed_maps_dir}/${SUBID}/${SUBID}_${vertex_type}_net${network}.dscalar.nii \
        ${outdir}/${SUBID}/${SUBID}_${vertex_type}.dlabel.nii \
        COLUMN \
        ${tmpdir}/${SUBID}_${vertex_type}_net${network}_${stat}.pscalar.nii \
        -method ${stat}

      wb_command -cifti-convert -to-text ${tmpdir}/${SUBID}_${vertex_type}_net${network}_${stat}.pscalar.nii ${outdir}/${SUBID}/${SUBID}_${vertex_type}_net${network}_${stat}.tsv
      done
    done
done
