#!/bin/bash

## set the site variable here
SITES="CALTECH
CMU
KKI
LEUVEN_1
LEUVEN_2
MAX_MUN
NYU
OLIN
PITT
SBL
SDSU
TRINITY
UCLA_1
UCLA_2
UM_1
UM_2
USM
YALE"


#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh

## run the rad4-8-12
PINToutputs=${SCRATCH}/ABIDE/20160610_PINT_Yeo7-76v_rad4-8-12

cd ${PINToutputs}

for SITE in $SITES; do
  SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
  parallel "echo ${HOME}/myscripts/abide/PINT/qc_PINT_Yeo7_v76_rad4-8-12.sh ${SITE} {}" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 04:00:00 -c 20 -j 4 --ppj 8 -N pintqc4${SITE} -
done


## run the rad6-6-12 version
PINToutputs=${SCRATCH}/ABIDE/20160610_PINT_Yeo7-76v_rad6-6-12

cd ${PINToutputs}

for SITE in $SITES; do
   SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
   parallel "echo ${HOME}/myscripts/abide/PINT/qc_PINT_Yeo7_v76_rad6-6-12.sh ${SITE} {}" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 04:00:00 -c 20 -j 4 --ppj 8 -N pintqc6${SITE} -
done
