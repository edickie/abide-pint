#!/bin/bash

## set the site variable here
SITES="CALTECH
CMU
KKI
LEUVEN_1
LEUVEN_2
MAX_MUN
NYU
OLIN
PITT
SBL
SDSU
TRINITY
UCLA_1
UCLA_2
UM_1
UM_2
USM
YALE"


#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abideII/epitome_env_nodatapaths.sh


## run the rad6-6-12 version
SamplingRad=6
SearchRad=6
PaddingRad=12
OutBase=${SCRATCH}/ABIDE/20161007_PINT_fixn
OutName="Yeo7-80v_rad${SamplingRad}-${SearchRad}-${PaddingRad}"
PINToutputs=${OutBase}_${OutName}

mkdir -p ${PINToutputs}/logs
cd ${PINToutputs}

for SITE in $SITES; do
  SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
  parallel "echo bash ${HOME}/myscripts/abide/PINT/find_PINT_Yeo7_v80_nadj_fix.sh ${SITE} {} ${SamplingRad} ${SearchRad} ${PaddingRad} ${OutBase}" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 01:30:00 -c 2 -j 2 --ppj 8 -N pint${SITE} -
done
