#!/bin/bash

## set the site variable here
SITE=${1}
SUBID=${2}
OutBase=${3}
InputVertices=${HOME}/myscripts/abideII/Yeo7_80verts_201608.csv
SamplingRad=6
SearchRad=6
PaddingRad=12
OutName="Yeo7-80v_rad${SamplingRad}-${SearchRad}-${PaddingRad}"

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
module load Xlibraries ImageMagick
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

export TMPDIR=${tmpdir}

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${TMPDIR} on "
    date
    rm -rf ${TMPDIR}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}

## now for the RSN-viewer
PINToutputs=${OutBase}_${OutName}
base_PINTout=$(basename ${PINToutputs})
qcdir="qc_${base_PINTout}"

echo $SUBID

epi-PINT-vis snaps\
  --qcdir ${TMPDIR}/${qcdir} \
  --roi-radius ${SamplingRad} \
  ${HCP_DATA}/${SUBID}/MNINonLinear/Results/rest_abide25fix/rest_abide25fix_Atlas_s8.dtseries.nii\
  ${SUBID} \
  ${PINToutputs}/${SUBID}/${SUBID}_${OutName}_summary.csv

cd ${TMPDIR}/${qcdir}
tar -cf ${SCRATCH}/ABIDE/${qcdir}/${SUBID}.tar ${SUBID}/
