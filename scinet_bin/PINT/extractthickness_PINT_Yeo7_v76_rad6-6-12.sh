#!/bin/bash

## set the site variable here
SITE=${1}
SUBID=${2}
InputVertices=${HOME}/myscripts/abide/Yeo7_76verts_201606.csv
SamplingRad=6
SearchRad=6
PaddingRad=12
OutName="Yeo7-76v_rad${SamplingRad}-${SearchRad}-${PaddingRad}"

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
module load Xlibraries ImageMagick
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

export TMPDIR=${tmpdir}

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${TMPDIR} on "
    date
    rm -rf ${TMPDIR}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}

## now for the RSN-viewer
PINToutputs=${SCRATCH}/ABIDE/20160610_PINT_${OutName}/${SUBID}/

echo $SUBID
ROIcifti=${TMPDIR}/${SUBID}rois.dscalar.nii

for verttype in "ivertex" "tvertex"; do
  ROIcifti=${TMPDIR}/${SUBID}_${verttype}rois.dscalar.nii

  epi-surface-rois \
    --vertex-col ${verttype} \
    --labels-col 'roiidx' \
    ${PINToutputs}/${SUBID}_${OutName}_summary.csv \
    ${SamplingRad} \
    ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.L.midthickness.32k_fs_LR.surf.gii \
    ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.R.midthickness.32k_fs_LR.surf.gii \
    ${ROIcifti}

  epi-meants --surfaceonly \
    --outputcsv ${PINToutputs}/${SUBID}_${OutName}_${verttype}_thickness.csv \
    --outputlabels ${PINToutputs}/${SUBID}_${OutName}_${verttype}_labelsthickness.csv \
    ${HCP_DATA}/${SUBID}/MNINonLinear/fsaverage_LR32k/${SUBID}.thickness.32k_fs_LR.dscalar.nii \
    ${ROIcifti}

done
