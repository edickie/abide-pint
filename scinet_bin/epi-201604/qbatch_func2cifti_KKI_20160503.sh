#!/bin/bash

## set the site variable here
SITE='KKI'
FIXTRAININSET=abide_KKI25
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
MASTERSCRIPT=master_160422_${SITE}_20160422.sh
PROCLIST=proclist_160422_${SITE}_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}



echo "Submitting the epi-qc job"
cd ${EPITOME_DATA}/${SITE}
echo ${HOME}/myscripts/abide/epi-201604-qc.sh ${SITE} ${PROCLIST} ${FIXTRAININSET}| \
  ${HOME}/code/qbatch/bin/qbatch --walltime 1:30:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} -

echo "Submitting the fs2hcp array"
cd ${HCP_DATA}
parallel "echo func2hcp \
  --InputfMRI=${EPITOME_DATA}/${SITE}/{}/RST/SESS01/func_MNI-nonlin.20160422.01.nii.gz \
  --HCPpath=${HCP_DATA} --Subject={} \
  --OutputBasename=rest_abide25fix --SmoothingFWHM=8 --DilationThreshold=4" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} -

# echo "Submitting the hcp-functional-qc"
# echo ${HOME}/myscripts/abide/abide-hcp-func-qc.sh ${SITE} | \
#   ${HOME}/code/qbatch/bin/qbatch --walltime 5:00:00 -c 1 -j 1 --ppj 8 -N func2hcpqc${SITE} -


