#!/bin/bash

## set the site variable here
SITE='UM_1'
FIXTRAININSET=abide_UM1_25
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
MASTERSCRIPT=master_160422_UM1_20160422.sh
PROCLIST=proclist_160422_UM1_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}


# ## get the subjects list from the NIIDIR
# NIIDIR=${SCRATCH}/ABIDE/inputs
#
# echo "Building epitome subject directories for:"
# for subject in ${SUBJECTS}; do
#   echo ${subject}
#   ## make the T1 directory
#   mkdir -p ${EPITOME_DATA}/${SITE}/${subject}/T1/SESS01/RUN01
#   ## make the rest direstory
#   mkdir -p ${EPITOME_DATA}/${SITE}/${subject}/RST/SESS01/RUN01
#   ## fslmerge the data and output into rest directory
#   cd ${EPITOME_DATA}/${SITE}/${subject}/RST/SESS01/RUN01
#   ln -s ${NIIDIR}/${subject}/${subject}/scans/rest/resources/NIfTI/files/rest.nii rest.nii
# done
# ## run epi-stage to create the directories
# #python ${HOME}/code/epitome/epitome/wip/epi-stage-abide.py --subjectlist ${SUBJECTLIST} ${DIR_NII} ${DIR_DATA}
#
# ## run epi-hcpexport for each site
cd ${EPITOME_DATA}/${SITE}
# echo "Running HCP  T1 data export..."
# epi-hcpexport --non-epi-subid ${EPITOME_DATA} ${SITE}
# echo "Done."
# ## run the masterscipt
#
# echo "Running the masterscript..."
# bash ${MASTERSCRIPT}

# # qbatch the magicks
# echo "Submitting all subjects jobs"
# cat ${PROCLIST} | grep cmd | grep UM_50308 | sed 's/bash //g' | \
#   ${HOME}/code/qbatch/bin/qbatch --walltime 4:00:00 -c 4 -j 4 --ppj 8 -N epi${SITE} -

# echo "Submitting the epi-qc job"
# echo ${HOME}/myscripts/abide/epi-201604-qc.sh ${SITE} ${PROCLIST} ${FIXTRAININSET}| \
#   ${HOME}/code/qbatch/bin/qbatch --walltime 1:30:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} --afterok epi${SITE} -

echo "Submitting the fs2hcp array"
cd ${HCP_DATA}
parallel "echo func2hcp \
  --InputfMRI=${EPITOME_DATA}/${SITE}/{}/RST/SESS01/func_MNI-nonlin.20160422.01.nii.gz \
  --HCPpath=${HCP_DATA} --Subject={} \
  --OutputBasename=rest_abide25fix --SmoothingFWHM=8 --DilationThreshold=4" ::: $SUBJECTS | \
    ${HOME}/code/qbatch/bin/qbatch --walltime 1:00:00 -c 4 -j 4 --ppj 8 -N func2hcp${SITE} -

echo "Submitting the hcp-functional-qc"
echo ${HOME}/myscripts/abide/abide-hcp-func-qc.sh ${SITE} | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 3:00:00 -c 1 -j 1 --ppj 8 -N func2hcpqc${SITE} --afterok func2hcp${SITE} -

echo "Submitting the RSN views"
echo ${HOME}/myscripts/abide/abide-hcp-RSN-view.sh ${SITE} | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 3:00:00 -c 1 -j 1 --ppj 8 -N RSNviews${SITE} --afterok func2hcp${SITE} -
