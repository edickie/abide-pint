#!/bin/bash
#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

## set the site variable here
SITE=UM_1

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
module load Xlibraries ImageMagick
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}
subjects=`ls -1d *_5* `

## now for the RSN-viewer
qcdir="qc_${SITE}_RSNviews"
parallel -j 4 "epi-RSN-vis --qcdir ${tmpdir}/${qcdir} --subject {} --snaps-only rest_abide25fix 8" ::: $subjects
epi-RSN-vis --qcdir ${tmpdir}/${qcdir} --index-only rest_abide25fix 8

## move the data from the ramdisk back to HCP_DATA
cd ${tmpdir}
tar -cf ${HCP_DATA}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}
cd ${HCP_DATA}
