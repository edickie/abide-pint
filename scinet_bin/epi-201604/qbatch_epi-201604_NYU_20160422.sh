#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=6:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20150822
## set the site variable here
SITE='NYU'
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`

#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

PROCLIST=proclist_160422_${SITE}_20160422.sh
cd ${EPITOME_DATA}/${SITE}

cat ${PROCLIST} | grep cmd | sed 's/bash //g' | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 3:00:00 -c 4 -j 4 --ppj 8 -N epi${SITE} - 

