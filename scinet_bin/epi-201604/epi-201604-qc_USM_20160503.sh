#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=2:00:00
#PBS -j oe

set -x

SITE=USM
proclist=proclist_160422_USM_20160422.sh
FIXTrainingSet=abide_USM25

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

epidir=${EPITOME_DATA}/${SITE}
qcdir="qc_icafix_${SITE}"
tmpdir=/dev/shm

### run ica_fix qc and copy all the qc to ramdisk then tar it up
cd ${epidir}
epi-qc-icafix --debug --copy-qcdir ${tmpdir}/${qcdir} --labelfilename fix4melview_${FIXTrainingSet}_thr20.txt */RST/SESS01/fake*feat/
cd ${tmpdir}
tar -cf ${epidir}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}

## generate the other qc pdfs
cd ${epidir}
cat ${proclist} | grep qc | parallel -j 4
