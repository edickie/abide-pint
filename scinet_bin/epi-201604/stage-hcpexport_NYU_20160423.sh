#!/bin/bash

## set the site variable here
SITE='NYU'
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`

#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}


## get the subjects list from the NIIDIR
NIIDIR=${SCRATCH}/ABIDE/inputs

for subject in ${SUBJECTS}; do
  ## make the T1 directory
  mkdir -p ${EPITOME_DATA}/${SITE}/${subject}/T1/SESS01/RUN01
  ## make the rest direstory
  mkdir -p ${EPITOME_DATA}/${SITE}/${subject}/RST/SESS01/RUN01
  ## fslmerge the data and output into rest directory
  cd ${EPITOME_DATA}/${SITE}/${subject}/RST/SESS01/RUN01
  ln -s ${NIIDIR}/${subject}/${subject}/scans/rest/resources/NIfTI/files/rest.nii rest.nii
done
## run epi-stage to create the directories
#python ${HOME}/code/epitome/epitome/wip/epi-stage-abide.py --subjectlist ${SUBJECTLIST} ${DIR_NII} ${DIR_DATA}

## run epi-hcpexport for each site
cd ${EPITOME_DATA}/${SITE}
epi-hcpexport --non-epi-subid ${EPITOME_DATA} ${SITE}

cd ${HOME}/myscripts/abide
