#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=17:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622 ImageMagick/6.6.7
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh
tmpdir=/dev/shm
qcdir="qc_MNIfsaverage32k"

SITES="CMU Leuven OHSU SBL Trinity USM Caltech MaxMun Olin SDSU	UCLA Yale KKI NYU Pitt Stanford UM"

for SITE in ${SITES}; do
  cd ${HCP_DATA}
  subjects=`ls -1d ${SITE}* `

  parallel -j 4 "epi-hcp-qc --qcdir ${tmpdir}/${qcdir} --subject {} --snaps-only MNIfsaverage32k" ::: $subjects

  cd ${tmpdir}/${qcdir}

  tar cf ${SITE}.tar *

  mkdir ${HCP_DATA}/${qcdir}

  cp ${tmpdir}/${qcdir}/${SITE}.tar ${HCP_DATA}/${qcdir}/${SITE}.tar

  rm -r ${tmpdir}/${qcdir}
done
