#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=8:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622 ImageMagick/6.6.7
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh
tmpdir=/dev/shm
qcdir="qc_native"


cd ${HCP_DATA}
subjects=`ls -1d * | head -100`

parallel -j 4 "epi-hcp-qc --qcdir ${tmpdir}/${qcdir} --subject {} --snaps-only native" ::: $subjects

cd ${tmpdir}/${qcdir}

tar cf ${qcdir}.tar *

mkdir ${HCP_DATA}/${qcdir}

cp ${tmpdir}/${qcdir}/${qcdir}.tar ${HCP_DATA}/${qcdir}/${qcdir}.tar

rm -r ${tmpdir}/${qcdir}
