#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

bash /scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/Caltech/Caltech_51456/cmd_160228_185517_scinet1.sh
