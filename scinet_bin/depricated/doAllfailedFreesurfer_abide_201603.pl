#!/usr/bin/env perl

use strict;
use File::Basename;
use MNI::Startup;
use Getopt::Tabular;

##############
# Define usage and help stuff
##############

my $me     = &basename($0);

#new usage:just run whole thing paths and options are hard coded into these scripts
my $Usage = "$me \n";
my $help =
"| $me run Freesurfer 5.3 recon-all on all of the abide dataset\n".
"| requires Freesurfer 5 enviroment \n\n";

##############
#Set up options
##############

########### sets the info the batch queue system will read  #########################
my $pbsinfo    = 	"#PBS -l nodes=1:ppn=8,walltime=24:00:00 \n". 	#sets the walltime
					"#PBS -j oe \n";  								#combines STDOUT and STDERR into one output log

############ sets the environment variables #########################################
my $enviroment = 	"module load gnu-parallel/20140622 \n".      # load gnu-parallel module to run 8 participants at a time
                  "module use -a /project/m/mchakrav/quarantine/modules \n"; #appends mallar;s labs enviroment

############ the parallel command ###################################################
my $inDir = '/scratch/a/arisvoin/edickie/ABIDE/inputs';
my $fs_subdir= '/scratch/a/arisvoin/edickie/ABIDE/freesurfer';
my $fs_env = '~/FS5.3.0_cobra_env.csh';
### Note: the {} character can be used to create a place holder for something we will list (i.e. subjectIDs)
my $gnuparallel_cmd  = "tcsh -c \\\"source $fs_env; recon-all -subject {} -i $inDir/{}/{}/scans/anat/resources/NIfTI/files/mprage.nii -sd $fs_subdir -all\\\"";

################## Two ways to input what will be iterated over:
#### 1: ($PIPE = 1) Pipe in a bash command
####		 -this can only be done if all subjects are proceses on the same node
####		 -little easier for fast jobs (like generating QC pages)
#### 2: ($PIPE = 0) Create and array in perl of what you want to iterate over
####		 - with this method you can specify how many subjects to send to each node
####		 - will also automatically run one subject by themselves (the "KEEPME.sh script)
####			so that one clean log can be kept for your records
####		 - better for any analysis you where you would like good records
####		 - better for any calculation taking longer than a minute (pipelines)

my $PIPE = 0;

#### If PIPE=0 Edit this section: Iterate over what?? (in this case $subid) ##############
# reads the subids from the folders in $fsDir that start with 0000*
my @subids;
foreach my $subid (split(/\n/, `cat $fs_subdir/failedsubs.txt`)){
	chomp($subid);
  push(@subids, $subid);
}

my @itOver = @subids;
my $numJ = 4;    #the number of subjects in each that will be in each script



#### If PIPE = 1, Edit this section: BASH command to pipe to parallel ###################

my $listing_cmd; # ='find /scratch/t/tpaus/edickie/IMAGEN_T1W/ -path *T1W.mnc';
#
# #but now I need the filenames for the qc index
# my @subids ;
# foreach my $mncFile (split(/\n/, `find /scratch/t/tpaus/edickie/IMAGEN_T1W/ -path *T1W.mnc`)){
# 	my $basename = &basename($mncFile);
# 	chomp($basename);
# 	$basename =~ s/.mnc//g;
# 	push(@subids, $basename);
# }

########### Set location of the folder that will hold the scripts and logs #############
my $scriptsDir = $fs_subdir."/qsubscripts2";


#################### write and submit scripts to the batch queue ####################

do_cmd('mkdir', '-p',$scriptsDir);
chdir($scriptsDir) or die;

my $scriptNum = 0;
my $i = 0;
my $iMax=0;

unless ($PIPE) {
	$iMax = $#itOver;
}

while ($i <= $iMax) {
    my $script;
    if ($i == 0 ) {
        $script = $scriptsDir."/"."KEEP_ME.sh";

    } else {
        $script = $scriptsDir."/"."script".$scriptNum.".sh";
    }
    open (WRITESCRIPT, ">$script");
    print WRITESCRIPT "#!/bin/bash\n\n";
    print WRITESCRIPT $pbsinfo."\n";
    print WRITESCRIPT "#script created by $me \n";
    print WRITESCRIPT $enviroment."\n";

    if ($PIPE) {
    	print WRITESCRIPT $listing_cmd.' | ';
    	print WRITESCRIPT "parallel --jobs 4 ";
    	print WRITESCRIPT "\"$gnuparallel_cmd\" \n";
    	$i++
    } else {
    	print WRITESCRIPT "parallel --jobs 4 ";
    	print WRITESCRIPT "\"$gnuparallel_cmd\" ::: ";
    	# writes one subid for the KEEPME.sh script or numJ subids for the rest
		if ($i == 0 ) {
		   print WRITESCRIPT "$itOver[$i] \n";
		   print "\n$script will process: $itOver[$i] \n";
		   $i++;
		} else {
		  print WRITESCRIPT "@itOver[($i)..($i + $numJ - 1)] \n";
		  print "\n$script will process: @itOver[($i)..($i + $numJ - 1)] \n";
		  $i = $i + $numJ;
		}
    }
    close (WRITESCRIPT);

    #print "\nWrote script $script \n";
    do_cmd(qw(chmod +x), $script);
    print "Submitting script $script \n";
    my $qsub = "qsub $script";
    #do_cmd($qsub);

    $scriptNum++;
}

unless ($PIPE) {
	print "\nSubmitted $#itOver subjects in $scriptNum scripts. \n\n";
}

############################# The classic do_cmd part ##################################
sub do_cmd
{
	print STDERR "@_ \n";
	system(@_) ==0 or die;
}
