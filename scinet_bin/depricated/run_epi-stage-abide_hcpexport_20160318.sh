#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

DIR_PIPE=/home/a/arisvoin/edickie/epi-clone/scinet1
DIR_NII=${SCRATCH}/ABIDE/inputs
DIR_DATA=${SCRATCH}/ABIDE/epi-fixtrain
SUBJECTLIST=${HOME}/myscripts/abide/icafix_trainingsublist2.txt

SITES="CMU Leuven OHSU SBL Trinity USM Caltech MaxMun Olin SDSU	UCLA Yale KKI NYU Pitt UM"

## run epi-stage to create the directories
python ${HOME}/code/epitome/wip/epi-stage-abide.py --subjectlist ${SUBJECTLIST} ${DIR_NII} ${DIR_DATA}

## run epi-hcpexport for each site
parallel --jobs 4 \
  "epi-hcpexport \
  --non-epi-subid \
  ${DIR_DATA} {}" ::: ${SITES}
