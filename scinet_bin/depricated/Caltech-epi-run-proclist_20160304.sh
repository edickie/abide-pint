#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh	

PROCLIST=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/Caltech/proclist_160228_185517_scinet1.sh

grep 'cmd' ${PROCLIST} | sed 's/bash //g' | \
  python /home/t/tpaus/kmarecko/code/qbatch/qbatch \
  --ppn 8 \
  -c 4 \
  --jobname CaltechPre \
  - \
  -o "-l walltime=4:00:00" 

