#!/bin/bash
#PBS -l nodes=1:ppn=8,walltime=5:00:00
#PBS -j oe

## set the site variable here
SITE=UCLA_2
InputVertices=${HOME}/myscripts/abide/Yeo7_76verts.csv
SamplingRad=5
SearchRad=10
OutName="Yeo7-76v_rad5-10"

#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20140622
module load Xlibraries ImageMagick
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

export TMPDIR=${tmpdir}

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${TMPDIR} on "
    date
    rm -rf ${TMPDIR}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

## get the subjects list from the HCP_DATA list
cd ${HCP_DATA}
subjects=`ls -1d *_5* `

## now for the RSN-viewer
PINToutputs=${SCRATCH}/ABIDE/PINT_testing/20160522_${SITE}_${OutName}
mkdir -p $PINToutputs

parallel -j 4 "find-PINT-vertices.py --pcorr --sampling-radius ${SamplingRad} --search-radius ${SearchRad} ${HCP_DATA}/{}/MNINonLinear/Results/rest_abide25fix/rest_abide25fix_Atlas_s8.dtseries.nii ${HCP_DATA}/{}/MNINonLinear/fsaverage_LR32k/{}.L.midthickness.32k_fs_LR.surf.gii ${HCP_DATA}/{}/MNINonLinear/fsaverage_LR32k/{}.R.midthickness.32k_fs_LR.surf.gii ${InputVertices} ${PINToutputs}/{}_${OutName} > ${PINToutputs}/{}_${OutName}.log" ::: $subjects


