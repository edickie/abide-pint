#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=5:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

PROCLIST=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/NYU/proclist_160318_164802_20160318.sh

cat ${PROCLIST} | grep cmd | sed 's/bash //g' | qbatch --walltime=3:00:00 -N NYU -c 4 -
