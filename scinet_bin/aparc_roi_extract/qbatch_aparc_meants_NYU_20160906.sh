#!/bin/bash
#PBS -l nodes=1:ppn=8,walltime=5:00:00
#PBS -j oe

## set the site variable here
SITE='NYU'
FIXTRAININSET=abide_NYU25
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
MASTERSCRIPT=master_160422_${SITE}_20160422.sh
PROCLIST=proclist_160422_${SITE}_20160422.sh


#load the epitome enviroment
module unload gnu-parallel
module load gnu-parallel/20150822
source ${HOME}/myscripts/abideII/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

# echo "Submitting the epi-qc job"
# cd ${EPITOME_DATA}/${SITE}
# echo ${HOME}/myscripts/abide/epi-201604-qc.sh ${SITE} ${PROCLIST} ${FIXTRAININSET}| \
#   ${HOME}/code/qbatch/bin/qbatch --walltime 1:30:00 -c 1 -j 1 --ppj 8 -N epiqc${SITE} -

echo "Submitting the epi-meants array"

TSOUTDIR=${HCP_DATA}/aparc_meants

mkdir -p $TSOUTDIR
cd ${HCP_DATA}


parallel -j 4 "epi-meants --surfaceonly --outputcsv ${TSOUTDIR}/{}_rest_abide25fix_Atlas_s8_aparc_meants.csv --outputlabels ${TSOUTDIR}/{}_rest_abide25fix_Atlas_s8_aparc_roiids.csv ${HCP_DATA}/{}/MNINonLinear/Results/rest_abide25fix/rest_abide25fix_Atlas_s8.dtseries.nii ${HCP_DATA}/{}/MNINonLinear/fsaverage_LR32k/{}.aparc.32k_fs_LR.dlabel.nii" ::: ${SUBJECTS}
