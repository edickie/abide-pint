#!/bin/bash

## set the site variable here
SITE='USM'

#load the epitome enviroment
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}


## get the subjects list from the NIIDIR
NIIDIR=${SCRATCH}/ABIDE/inputs
SUBJECTS=`cd ${NIIDIR}; ls -1d $SITE*`
## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir $HCP_DATA
cd $HCP_DATA
parallel "echo fs2hcp --FSpath=$SUBJECTS_DIR --HCPpath=$HCP_DATA --subject={}" ::: $SUBJECTS | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 2:00:00 -c 4 -j 4 --ppj 8 -N fs2hcp${SITE} -
