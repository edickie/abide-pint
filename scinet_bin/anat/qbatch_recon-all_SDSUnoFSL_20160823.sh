#!/bin/bash

## set the site variable here
SITE='SDSU'
SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/old_lists/${SITE}_MRids_all.txt`

#load the epitome enviroment
module load gnu-parallel/20150822
source /home/t/tpaus/tpaus/opt/environments/freesurfer5.3.0_env.sh
module use -a ${HOME}/quarantine/modules
module load edickie_quarantine
module load python/2.7.11-Anaconda2-2.5.0

export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout_noFSL/${SITE}

## get the subjects list from the NIIDIR
NIIDIR=${SCRATCH}/ABIDE/inputs

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {} -i ${NIIDIR}/{}/{}/scans/anat/resources/NIfTI/files/mprage.nii -sd ${SUBJECTS_DIR} -all" ::: $SUBJECTS | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 24:00:00 -c 4 -j 4 --ppj 8 -N fs${SITE} -
