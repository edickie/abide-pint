#!/bin/bash

## set the site variable here
SITE='SDSU'
SUBJECTS="SDSU_50182
SDSU_50183
SDSU_50184
SDSU_50185
SDSU_50186
SDSU_50187
SDSU_50188
SDSU_50189
SDSU_50197"

#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

## remove the older outputs
for SUBJECT in $SUBJECTS; do
  rm -r ${SUBJECTS_DIR}/${SUBJECT}/
  rm -r ${HCP_DATA}/${SUBJECT}/
done

## get the subjects list from the NIIDIR
NIIDIR=${SCRATCH}/ABIDE/inputs

## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {} -i ${NIIDIR}/{}/{}/scans/anat/resources/NIfTI/files/mprage.nii -sd ${SUBJECTS_DIR} -all" ::: $SUBJECTS | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 30:00:00 -c 4 -j 4 --ppj 8 -N fsredo${SITE} -
