#!/bin/bash

## set the site variable here
SITE='UM_1'
SUBJECTS="UM_50272
UM_50273
UM_50274
UM_50276
UM_50277
UM_50287
UM_50288
UM_50327
UM_50328"

#load the epitome enviroment
module load gnu-parallel/20150822
source ${HOME}/myscripts/abide/epitome_env_nodatapaths.sh
export EPITOME_DATA=${SCRATCH}/ABIDE/epi-201604
export SUBJECTS_DIR=${SCRATCH}/ABIDE/FSout/${SITE}
export HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}

## get the subjects list from the NIIDIR
NIIDIR=${SCRATCH}/ABIDE/inputs

## do the fixing of the orientations
for sub in ${SUBJECTS}; do
  rm -r ${SUBJECTS_DIR}/${sub}
  rm -r ${HCP_DATA}/${sub}
  cd ${NIIDIR}/${sub}/${sub}/scans/anat/resources/NIfTI/files/
  ### previously ran
  # mv mprage.nii mprage_badorientation.nii.gz
  fslswapdim mprage_badorientation.nii z -x y mprage_tmp.nii.gz
  fslorient -deleteorient mprage_tmp.nii.gz
  ## run python script that copies the head from the old image
  python ${HOME}/myscripts/abide/nibabelloadandresave.py
done


## add now the magicks happen...pipes the subject list to the fs2hcp command to qbatch...
mkdir -p $SUBJECTS_DIR
cd $SUBJECTS_DIR
parallel "echo recon-all -subject {} -i ${NIIDIR}/{}/{}/scans/anat/resources/NIfTI/files/mprage.nii.gz -sd ${SUBJECTS_DIR} -all" ::: $SUBJECTS | \
  ${HOME}/code/qbatch/bin/qbatch --walltime 30:00:00 -c 4 -j 4 --ppj 8 -N fsredo2${SITE} -
