ABIDE dataset of resting state imaging in Autism

##This dataset is publically available:
http://fcon_1000.projects.nitrc.org/indi/abide/


## Processing step so far...

## before running freesurfer I needed to create some simlinks 
## for a few problem subjects so that their anat was named similar to the rest
hiresSUBIDS="UCLA_51232 UCLA_51233 UCLA_51242 UCLA_51243 UCLA_51244 UCLA_51245 UCLA_51246 UCLA_51247 UCLA_51270 UCLA_51310"

for subid in $hiresSUBIDS; do
  mkdir -p $subid/$subid/scans/anat/resources/NIfTI/files/
  ln -s  $subid/$subid/scans/hires/resources/NIfTI/files/hires.nii $subid/$subid/scans/anat/resources/NIfTI/files/mprage.nii
done

## freesurfer 5.3.0 run on scinet using script
~/myscripts/abide/doAllFreesurfer_abide_201601.pl
epitome_env.sh lists all the modules and enviroment variables for this project
