## scinet modules

## edickie's modules
module use -a ${HOME}/quarantine/modules
module load edickie_quarantine
module load freesurfer/5.3.0
module load FSL/5.0.9
module load Xlibraries ## this is now a prereq for connectome-workbench
module load connectome-workbench/1.1.1
module load hcp-pipelines/3.13.1
module load python/2.7.11-Anaconda2-2.5.0

export PATH=${PATH}:${HOME}/code/ciftify/bin
export PYTHONPATH=${PYTHONPATH}:${HOME}/code/ciftify/

export HCP_SCENE_TEMPLATES=${HOME}/code/ciftify/data/scene_templates
