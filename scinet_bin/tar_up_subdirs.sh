#!/bin/bash

set -xe

## read in the directory path to tar as argument 1
THISDIR=${1}

## cd to the directory
cd ${THISDIR}

## using the ABIDE subject prefix as what we want
SUBJECTS=`ls -1d *_5????`

## now do the shiz
for SUB in ${SUBJECTS}; do
  tar --create --file=${SUB}.tar ${SUB}/
  tar --compare --file=${SUB}.tar ${SUB}/
  rm -r ${SUB}/
done
