#!/bin/bash

HCP_DATA=${1}
SUBJECT=${2}
fMRIName=${3}

echo Deleting Struct tmp files from ${HCP_DATA}/${SUBJECT}

rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/ROIs/Atlas_wmparc.2.nii.gz
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/ROIs/wmparc.2.nii.gz

rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.aparc.native.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.BA.native.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.aparc.a2009s.native.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.sulc.native.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.thickness.native.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.curvature.native.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Native/${SUBJECT}.?.ArealDistortion_FS.native.shape.gii

rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.aparc.164k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.BA.164k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.aparc.a2009s.164k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.sulc.164k_fs_LR.shape.gii   
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.thickness.164k_fs_LR.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.curvature.164k_fs_LR.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/${SUBJECT}.?.ArealDistortion_FS.164k_fs_LR.shape.gii

rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.aparc.32k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.BA.32k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.aparc.a2009s.32k_fs_LR.label.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.sulc.32k_fs_LR.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.thickness.32k_fs_LR.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.curvature.32k_fs_LR.shape.gii
rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/fsaverage_LR32k/${SUBJECT}.?.ArealDistortion_FS.32k_fs_LR.shape.gii

echo Deleting fMRI tmp files from ${HCP_DATA}/${SUBJECT} folder ${fMRIName}
## All of ribbon to surface mapping..
if [ `echo ${fMRIName}` != '' ]; then
  rm -r ${HCP_DATA}/${SUBJECT}/MNINonLinear/Results/${fMRIName}/RibbonVolumeToSurfaceMapping/
  rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Results/${fMRIName}/${fMRIName}_s?.atlasroi.?.32k_fs_LR.func.gii
  rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Results/${fMRIName}/${fMRIName}.?.native.func.gii
  rm ${HCP_DATA}/${SUBJECT}/MNINonLinear/Results/${fMRIName}/${fMRIName}_AtlasSubcortical_s8.nii.gz
fi
