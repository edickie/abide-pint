#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/Leuven/
OUTPUT="abide_Leuven25_2"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="Leuven_50723
Leuven_50724
Leuven_50725
Leuven_50726
Leuven_50727
Leuven_50728
Leuven_50730
Leuven_50732
Leuven_50734
Leuven_50736
Leuven_50737
Leuven_50738
Leuven_50739
Leuven_50741
Leuven_50742
Leuven_50743
Leuven_50744
Leuven_50746
Leuven_50747
Leuven_50748
Leuven_50750
Leuven_50751
Leuven_50752
Leuven_50754
Leuven_50755"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
