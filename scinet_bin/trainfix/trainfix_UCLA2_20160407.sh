#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/UCLA/
OUTPUT="abide_UCLA23_2"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="UCLA_51291
UCLA_51292
UCLA_51293
UCLA_51294
UCLA_51295
UCLA_51297
UCLA_51298
UCLA_51299
UCLA_51300
UCLA_51301
UCLA_51302
UCLA_51303
UCLA_51304
UCLA_51305
UCLA_51306
UCLA_51307
UCLA_51308
UCLA_51309
UCLA_51311
UCLA_51313
UCLA_51314
UCLA_51315
UCLA_51317"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
