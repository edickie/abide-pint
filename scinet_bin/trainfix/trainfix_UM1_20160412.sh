#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/UM1/
OUTPUT="abide_UM1_25"

cd ${BASEDIR}

## get a list of all the .ica directories
icadirs=""
for hl in */RST/SESS01/fake.*.feat/hand_labels_noise.txt; do
  icadir=$(dirname $hl)
  icadirs="$icadirs $icadir"
done

## doing features in serial because me stupid
# for icadir in $icadirs; do
#   fix -f $icadir;
# done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs

