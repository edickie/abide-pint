#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/UCLA/
OUTPUT="abide_UCLA25_1"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="UCLA_51202
UCLA_51204
UCLA_51207
UCLA_51209
UCLA_51211
UCLA_51212
UCLA_51215
UCLA_51218
UCLA_51219
UCLA_51226
UCLA_51229
UCLA_51231
UCLA_51236
UCLA_51248
UCLA_51251
UCLA_51255
UCLA_51263
UCLA_51264
UCLA_51265
UCLA_51266
UCLA_51267
UCLA_51275
UCLA_51276
UCLA_51279
UCLA_51280"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
