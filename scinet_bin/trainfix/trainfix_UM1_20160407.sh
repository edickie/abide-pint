#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/UM/
OUTPUT="abide_UM25_1"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="UM_50284
UM_50285
UM_50286
UM_50289
UM_50290
UM_50294
UM_50300
UM_50302
UM_50315
UM_50318
UM_50326
UM_50329
UM_50343
UM_50346
UM_50349
UM_50350
UM_50353
UM_50354
UM_50363
UM_50366
UM_50371
UM_50372
UM_50375
UM_50377
UM_50379"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
