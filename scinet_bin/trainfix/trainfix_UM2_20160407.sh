#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/UM/
OUTPUT="abide_UM25_2"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="UM_50382
UM_50383
UM_50385
UM_50386
UM_50387
UM_50388
UM_50391
UM_50397
UM_50399
UM_50402
UM_50404
UM_50405
UM_50406
UM_50407
UM_50408
UM_50410
UM_50411
UM_50413
UM_50414
UM_50415
UM_50416
UM_50419
UM_50424
UM_50427
UM_50428"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
