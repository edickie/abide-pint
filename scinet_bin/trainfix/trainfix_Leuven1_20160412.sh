#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=4:00:00
#PBS -j oe

#script my new fancy enviroment
module use -a /home/a/arisvoin/edickie/quarantine/modules
module load edickie_quarantine FSL/5.0.9 FIX/1.06

BASEDIR=${SCRATCH}/ABIDE/epi-fixtrain/Leuven/
OUTPUT="abide_Leuven25_1"

cd ${BASEDIR}

## get a list of all the .ica directories
subjects="Leuven_50682
Leuven_50683
Leuven_50685
Leuven_50686
Leuven_50687
Leuven_50688
Leuven_50689
Leuven_50690
Leuven_50691
Leuven_50692
Leuven_50693
Leuven_50695
Leuven_50696
Leuven_50699
Leuven_50700
Leuven_50701
Leuven_50702
Leuven_50703
Leuven_50704
Leuven_50705
Leuven_50706
Leuven_50707
Leuven_50709
Leuven_50710
Leuven_50711"

## make list of icadirs for fix out of list
icadirs=""
for subject in $subjects; do
  icadir=${subject}/RST/SESS01/fake.20160319.01.feat
  icadirs="$icadirs $icadir"
done

## run the training step with LOO
fix -t ${OUTPUT} -l $icadirs
