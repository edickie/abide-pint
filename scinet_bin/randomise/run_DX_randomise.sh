#!/bin/bash

vertex_type=${1}
network=${2}
seedmaps_dir=${3}

source ~/myscripts/abide/ciftify_env_nodatapaths.sh

cd ${seedmaps_dir}

#mkdir average_allQCed
subjects=`cat ~/myscripts/abide/randomise/ABIDEI_glm889_DX_subids.txt`
design=${HOME}/myscripts/abide/randomise/ABIDEI_glm889_DX.mat
con=${HOME}/myscripts/abide/randomise/ABIDEI_glm889_DX.con
nsubs=889
mask=ABIDEI_glm889_inputs/mask_n${nsubs}_subcortical.nii.gz


## create the input 4D file
ciftilist=""
for subject in $subjects; do
   ciftilist="$ciftilist -cifti ${subject}/${subject}_${vertex_type}_net${network}.dscalar.nii"
done
wb_command -cifti-merge ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}.dscalar.nii $ciftilist
wb_command -cifti-separate \
  ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}.dscalar.nii \
  COLUMN \
  -volume-all ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}_subcortical.nii.gz

## run randomise
randomise -i ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}_subcortical.nii.gz \
   -o randomise/DX_${vertex_type}_net${network}_n${nsubs} \
   -d ${design} -t ${con} -m ${mask} -T
