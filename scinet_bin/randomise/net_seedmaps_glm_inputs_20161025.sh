
source ~/myscripts/abide/ciftify_env_nodatapaths.sh

cd ${SCRATCH}/ABIDE/20161007_PINT_fixn_Yeo7-80v_rad6-6-12_seedmaps

#mkdir average_allQCed
subjects=`cat ~/myscripts/abide/randomise/ABIDEI_glm889_DX_subids.txt`
design=${HOME}/myscripts/abide/randomise/ABIDEI_glm889_DX.mat
con=${HOME}/myscripts/abide/randomise/ABIDEI_glm889_DX.con
nsubs=889

mkdir ABIDEI_glm889_inputs

for vertex_type in ivertex; do
  for network in 2; do
    ciftilist=""
    for subject in $subjects; do
       ciftilist="$ciftilist -cifti ${subject}/${subject}_${vertex_type}_net${network}.dscalar.nii"
    done
    wb_command -cifti-merge ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}.dscalar.nii $ciftilist
    wb_command -cifti-separate \
      ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}.dscalar.nii \
      COLUMN \
      -volume-all ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}_subcortical.nii.gz
   done
done

ciftilist=""
for subject in $subjects; do
   ciftilist="$ciftilist -cifti ${subject}/${subject}_mask.dscalar.nii"
done
wb_command -cifti-merge ABIDEI_glm889_inputs/merged_mask_n${nsubs}.dscalar.nii $ciftilist


##
wb_command -cifti-reduce \
   ABIDEI_glm889_inputs/merged_mask_n${nsubs}.dscalar.nii \
   MIN \
   ABIDEI_glm889_inputs/mask_n${nsubs}.dscalar.nii

wb_command -cifti-separate \
  ABIDEI_glm889_inputs/mask_n${nsubs}.dscalar.nii \
  COLUMN \
  -volume-all ABIDEI_glm889_inputs/mask_n${nsubs}_subcortical.nii.gz


mask=ABIDEI_glm889_inputs/mask_n${nsubs}_subcortical.nii.gz
mkdir test_rando
vertex_type="ivertex"
network=2
randomise -i ABIDEI_glm889_inputs/merged_${vertex_type}_net${network}_n${nsubs}_subcortical.nii.gz \
 -o test_rando/DX_${vertex_type}_net${network}_n${nsubs} -d ${design} -t ${con} -m ${mask} -n 100 -T



cd ${SCRATCH}/ABIDE/20161007_PINT_fixn_Yeo7-80v_rad6-6-12_seedmaps/randomise
module load gnu-parallel/20150822
parallel "echo bash ${HOME}/myscripts/abide/randomise/run_DX_randomise.sh {1} {2}" ::: ivertex tvertex ::: 2 3 4 5 6 7 | \
 ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 06:00:00 -c 1 -j 1 --ppj 8 -N DXrando -

parallel "echo bash ${HOME}/myscripts/abide/randomise/run_minDX_randomise.sh {1} {2}" ::: ivertex tvertex ::: 2 3 4 5 6 7 | \
 ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 06:00:00 -c 1 -j 1 --ppj 8 -N minDXrando -

parallel "echo bash ${HOME}/myscripts/abide/randomise/run_oneT_randomise.sh {1} {2}" ::: ivertex tvertex ::: 2 3 4 5 6 7 | \
 ${HOME}/code/qbatch/bin/qbatch --no-env --header "module load gnu-parallel/20150822" --walltime 10:00:00 -c 1 -j 1 --ppj 8 -N oneTrando -
