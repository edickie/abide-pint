## how to measure homogenetity

## method 1 - calculate a dconn based gradient map??
then reduce it by the ivertex or tvertex roi

## method 2 - calculate a
reduce the network calculated map by the network signature..
then reduce it by the ivertex or tvertex roi..

we wrote a script extract_homogenetity_stats.sh to do both...

WE run it using

note:test jobs started around ..11:13 - finished 11:32 therefore takes ~20min

```sh
module load ~/quarantine/modules/ciftify/v1.0.0
module load gnu-parallel/20170422

export CIFTIFY_TEMPLATES=${HOME}/code/ciftify/ciftify/data

PINT_outputdir=${SCRATCH}/ABIDE/20161101_PINT_Yeo7-80v_rad6-6-12
seed_maps_dir=${SCRATCH}/ABIDE/20161101_PINT_Yeo7-80v_rad6-6-12_seedmaps
PINT_outname="Yeo7-80v_rad6-6-12"       # the string in the PINT output names
PINT_sampling_rad=6   # the sampling radius
mem_limit=8           # memory limit option for the gradient map creation
outdir=${SCRATCH}/ABIDE/20161101_PINT_Yeo7-80v_rad6-6-12_homogenety_stats

mkdir ${outdir}
cd ${outdir}

## set the site variable here
SITES="CALTECH
CMU
KKI
LEUVEN_1
LEUVEN_2
MAX_MUN
NYU
OLIN
PITT
SBL
SDSU
TRINITY
UCLA_1
UCLA_2
UM_1
UM_2
USM
YALE"

for SITE in ${SITES}; do
HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}
SUBIDS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`

parallel "echo bash ${HOME}/myscripts/abide/PINT/extract_homogenetity_stats.sh {} \
  ${HCP_DATA} \
  ${PINT_outputdir} \
  ${seed_maps_dir} \
  ${PINT_outname} \
  ${PINT_sampling_rad} \
  ${mem_limit}\
  ${outdir}" ::: ${SUBIDS} |\
  qbatch --walltime 00:40:00 -c 1 -j 1 --ppj 8 -N pintho${SITE} -
done
```
