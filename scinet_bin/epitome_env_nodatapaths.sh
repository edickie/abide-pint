## scinet modules


## COBR lab modules
#module use -a /project/m/mchakrav/quarantine/modules
#module load quarantine freesurfer/5.3.0
source /home/t/tpaus/tpaus/opt/environments/freesurfer5.3.0_env.sh

## edickie's modules
module use -a ${HOME}/quarantine/modules
module load edickie_quarantine
module load FSL/5.0.9
module load AFNI/2015.12.15
module load FIX/1.06  ## note fix will purge then load other modules in a subshell
module load connectome-workbench/1.1.1
module load hcp-pipelines/3.13.1
module load python/2.7.11-Anaconda2-2.5.0

export PATH=${PATH}:${HOME}/code/epitome/bin
export PYTHONPATH=${PYTHONPATH}:${HOME}/code/epitome/ 

export EPITOME_CLONE=${HOME}/epi-clone
