#!/bin/bash

set -x

## read in the directory path to tar as argument 1
SITE=${1}

HCP_DATA=${SCRATCH}/ABIDE/hcp/${SITE}
EPIDIR=${SCRATCH}/ABIDE/epi-201604/${SITE}

## cd to the directory
cd ${EPIDIR}

## using the ABIDE subject prefix as what we want
SUBJECTS=`ls -1d *_5????`

## cp the qc data to the hcpdir
EPIQCCOPY=${HCP_DATA}/epi-201604-QC
mkdir ${EPIQCCOPY}
cp ${EPIDIR}/qc_icafix_*.tar  ${EPIQCCOPY}/
cp ${EPIDIR}/ica_fix_report_*.csv ${EPIQCCOPY}/
cp ${EPIDIR}/qc_reg_EPI_to_T1_RST.pdf ${EPIQCCOPY}/

## now do the shiz
for SUB in ${SUBJECTS}; do
  ## copy the params and the native space preprocessed image to the hcp directory
  PARAMSDIR="${HCP_DATA}/${SUB}/MNINonLinear/Results/rest_abide25fix/PARAMS"
  NATIVEDIR="${HCP_DATA}/${SUB}/MNINonLinear/Results/rest_abide25fix/native_space"
  mkdir ${NATIVEDIR}/
  cp ${EPIDIR}/${SUB}/RST/SESS01/func_fix.20160422.01.nii.gz ${NATIVEDIR}/
  mkdir ${PARAMSDIR}/
  cp ${EPIDIR}/${SUB}/RST/SESS01/PARAMS/* ${PARAMSDIR}/

  ## rm the files we don't really need to back-up
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_brain.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_mask_MNI-lin.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_mask_MNI-nonlin.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_tmp_mean.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_tmp_vol.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_EPI_ts_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/anat_tmp_smoothmask.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_deskull.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_despike.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_motion.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_scaled.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_RAI.20160422.01.nii
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_detrend.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_tmp_mean.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/func_volsmooth.20160422.01.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat_EPI_to_T1.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat_T1_to_EPI.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat_T1_to_TAL.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat_TAL_to_EPI.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/mat_TAL_to_T1.mat
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg_EPI_to_T1.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg_T1_to_EPI.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg_T1_to_TAL.nii.gz
  rm ${EPIDIR}/${SUB}/RST/SESS01/reg_nlin_TAL.nii.gz
  rm -r ${EPIDIR}/${SUB}/RST/SESS01/MELODIC*

done
