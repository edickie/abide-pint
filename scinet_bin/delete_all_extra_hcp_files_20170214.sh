SITES="CALTECH
CMU
KKI
LEUVEN_1
LEUVEN_2
MAX_MUN
NYU
OLIN
PITT
SBL
SDSU
TRINITY
UCLA_1
UCLA_2
UM_1
UM_2
USM
YALE"

for SITE in $SITES; do
  SUBJECTS=`cat ${HOME}/myscripts/abide/subject_lists/${SITE}_MRids.txt`
  for SUBJECT in $SUBJECTS; do
    bash ~/myscripts/abide/delete_extra_hcp_files.sh ${SCRATCH}/ABIDE/hcp/${SITE} ${SUBJECT} rest_abide25fix
  done
done
