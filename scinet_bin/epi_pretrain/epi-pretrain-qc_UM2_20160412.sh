#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=2:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

site="UM2"
proclist="proclist_160319_104903_20160319.sh"
epidir=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/${site}

tmpdir=$(mktemp --tmpdir=/dev/shm -d tmp.XXXXXX)

function cleanup_ramdisk {
    echo -n "Cleaning up ramdisk directory ${tmpdir} on "
    date
    rm -rf ${tmpdir}
    echo -n "done at "
    date
}

#trap the termination signal, and call the function 'trap_term' when
# that happens, so results may be saved.
trap cleanup_ramdisk EXIT

### run ica_fix qc and copy all the qc to ramdisk then tar it up
cd ${epidir}
qcdir="qc_icafix"
epi-qc-icafix --copy-qcdir ${tmpdir}/${qcdir} */RST/SESS01/fake*feat/
cd ${tmpdir}
tar -cf ${epidir}/${qcdir}.tar ${qcdir}/

## generate the other qc pdfs
cd ${epidir}
cat ${proclist} | grep qc | parallel -j 4
