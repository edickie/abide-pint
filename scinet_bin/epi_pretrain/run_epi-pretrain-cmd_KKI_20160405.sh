#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=9:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

PROCLIST=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/KKI/proclist_160319_104asc_20160319.sh

cat ${PROCLIST} | grep cmd | sed 's/bash //g' | parallel -j 4
