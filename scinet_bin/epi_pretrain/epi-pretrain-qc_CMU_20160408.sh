#!/bin/bash

#PBS -l nodes=1:ppn=8,walltime=2:00:00
#PBS -j oe

#script created by doAllfs2hcp_abide_201602.pl
module load gnu-parallel/20140622 
source ${HOME}/myscripts/abide/epitome_env_epi-fixtrain.sh

site="CMU"

epidir=/scratch/a/arisvoin/edickie/ABIDE/epi-fixtrain/${site}
prolist=${epidir}/proclist_160319_104903_20160319.sh
qcdir="qc_icafix_${site}"
tmpdir=/dev/shm

### run ica_fix qc and copy all the qc to ramdisk then tar it up
cd ${epidir}
epi-qc-icafix --copy-qcdir ${tmpdir}/${qcdir} */RST/SESS01/fake*feat/
cd ${tmpdir}
tar -cf ${epidir}/${qcdir}.tar ${qcdir}/
rm -r ${tmpdir}/${qcdir}

## generate the other qc pdfs
cd ${epidir}
cat ${proclist} | grep qc | parallel -j 4
