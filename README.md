# The PINT ABIDE project

# organization of files/folder in this project

+ **reports** : R markdown reports doing most of the final stats
+ **scripts** : R, python and bash scripts that were important for some bit of the processing
+ **scinet_bin** : synced folder directory of everything run on the SciNet HPC cluster
+ **data** (currently git-ignored) : extracted derivates required by the notebooks in `reports`
   + to download data files go to https://drive.google.com/open?id=1yDiDKvFnatxQj9KPhZHVJBtsVEjAZ2LH
+ **I2C2 software** (from M. Lundquist)
+ **phenotypic_and_qc** : manual qc ratings and Yeo templates
+ **plots** (not version controlled)
+ **tmp_Rdata** (not version controlled)

## reports/

  + **combine_qap_ica_into_qced_subjectlists.Rmd**
     + Exclusion of poor quality subjects and PCA of QAP parameters
     + outputs intermediate phenotypic table in all analyses)
  + **feb142017_abide1_report.Rmd** : the main results of the paper (using ABIDE I)
  + **201705_abideI_report.Rmd** (never knitted) : contains some updated plots of main results
  + **longitudinal_testretest_20170120.Rmd** : runs test-retest stats for ABIDE Longitudinal
  + **all_testrest_20171118.Rmd** : generates plots from CoRR sample in Figure 2
  + **test_restest_part2_20171201.Rmd** : generates supplemental figures and table re: CoRR
  + **calc_homogenetity_stats.Rmd** : the homoegeity stats reported in supplemental figure S6
  + **circlize_plots_20170523.Rmd** : generates circlize plots from Figure 4
  + **dec2016_report_age_models.Rmd** : test several ways to model age as covariate
  + **latextify_tables.Rmd** : does latex formatting on tables in supplement
  + **qap_oldvsnew_20161007.Rmd** : compares QAP run in our lab to public values
  + **representative_timeseries_for_methods_figure.Rmd** : creats timeseries used in Figure 1
  + **table_from_PINT_logs_20170322.Rmd** : parses logs to generate PINT performance stats

## scripts

  + **brain_coord_graph_abideI.ipynb** : build nilearn plots in figure 4
  + **build_randomise_models.R** : builds ABIDE I design matrix for randomise (results not reported)
  + **format_distances_for_jdv_classifier_20170214.R** : format pint results for jdv's xbrain
  + **get_tvertex_distance_mat.Rmd** : some stats on how far the template ROIs are from each other  
  + **longitudinal_figure.sh** : create maps used for schematic in Figure 2
  + **PINT_corrmats_fromcorfile_20170214.py** : generates average correlation matrices heatmaps in supplemental figure.
  + **prep_concatresult_for_premutations_ageU31.R** :
prep_concatresult_for_premutations.R
  + **rep_subject_pcorr_maps.ipynb** : notebook for creating pcorr maps in representative subject for Figure 1
  + **representative_subject_plots.ipynb** : notebook for creating pcorr maps in representative subject for Figure 1
  + **result_to_adjacency_dxresutls_20170523.R** : converts a tidy results table back to an adgacency matrix
  + **UPSM_probmaps.ipynb** : makes a UPSM surface PINT probability map for Figure 2 background
  + **vertex_permutation_posttest.sh** : some extra lines that need to be run after permutation tests
  + **vertex_probmaps..** : notebook used to create vertex-wise probability maps of the PINT ROI locations

`deprecated/`

  + **calc_between_sub_distances_UPSM_Long_6-6-12_20160906.py**
  + **calc_between_subject_distance_20161101.ipynb**
  + **concatenate_abide_80v_20161101.R** : concatenates the pint outputs (not now postPINT1 does this)

## compressed data (extract into data/)

The compressed data folders contents can be downloaded from:
https://drive.google.com/drive/folders/1yDiDKvFnatxQj9KPhZHVJBtsVEjAZ2LH?usp=sharing

They contain:

### The PINT ouputs from the Test-Retest datasets (reported in Figure 2)

+ **20161101_PINT_fixn_UPSM_Long_Yeo7-80v_rad6-6-12.tgz**
  + The PINT outputs from UPSM site of ABIDE-Longitudinal
+ **20161101_PINT_UCLA_Longitudinal_Yeo7-80v_rad6-6-12.tgz**
  + The PINT outputs from UCLA site of ABIDE-Longitudinal
+ **CoRR_testretest.tgz**
  + The PINT outputs from the CoRR datasets

### The PINT outputs from ABIDI

+ **20161101_PINT_Yeo7-80v_rad6-6-12.tgz**
  + The PINT outputs for the ABIDEI dataset. Used for generate the main and many supplemental analysis.

### Out data files:

+ **FSout.tgz**
  + freesurfer derived data, contains surface area metrics used a covariate in distance analysis
  + the distance analysis results are reported in Table 1 of the main paper
+ **PINT_fix_corr.tgz**
  + PINT run on ABIDEI using full correlation within the algorithm,
  + as shown in suppl figure S11 these settings led to stronger head-motion effects of intrinsic correlations
+ **20161101_PINT_Yeo7-80v_rad6-6-12_homogenety_stats.tgz**
  + data for the homogenity analyses reported in suppl figure S6
+ **roi43_DXcodes_U31.tgz**
   + outputs for the permutation analysis reported in figure 3
