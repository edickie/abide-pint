---
title: "tvertex distance graph"
output:
  pdf_document: default
  html_notebook: default
---



```{r}
library(readr)
library(dplyr)
library(tidyr)
library(purrr)
library(stringr)
library(ggplot2)
```

```{r}
read_distance_mat <- function(subid, basedir) {
  path = file.path(basedir, str_c(subid, '_Yeo7_2011_tvertex_distancemat.csv'))
  distance_mat <- read_csv(path,
                           col_types = c(
                             .default = col_double(),
                             X1 = col_integer(),
                             hemi = col_character(),
                             tvertex = col_integer(),
                             LRpairs = col_integer(),
                             roiidx = col_integer(),
                             NETWORK = col_integer(),
                             LOBE = col_character(),
                             SHORTNAME = col_character(),
                             x = col_integer(),
                             y = col_integer(),
                             z = col_integer()))
  return(distance_mat)
}
```
```{r}
ABIDEI_Pheno_QCed_20161010 <- read_csv("../phenotypic_and_qc/ABIDEI_Pheno_QCed_20161010.csv")
tvertex_outputs <- '../data/tvertex_distances/'
Yeo7_80verts_201704 <- read_csv("../phenotypic_and_qc/Yeo7_80verts_201704.csv")

```
# Reading and summarising all distances

```{r warning=FALSE}
all_distances <- ABIDEI_Pheno_QCed_20161010 %>%
  select(subid) %>%
  mutate(distances = map(subid, ~read_distance_mat(.x, tvertex_outputs))) %>%
  unnest()

median_distances <- all_distances %>%
  gather(roiidy, mm, ends_with('_dist')) %>%
  group_by(roiidx, roiidy) %>%
  summarise(median_mm = median(mm)) 

median_distances1 <- median_distances %>%
  rename(from_roiidx = roiidx) %>%
  mutate(to_roiidx = str_replace(roiidy,'roiidx','') %>%
           str_replace('_dist','') %>% parse_number()) %>%
  inner_join(Yeo7_80verts_201704, by = c("from_roiidx" = "roiidx")) %>%
  inner_join(Yeo7_80verts_201704, by = c("to_roiidx" = "roiidx"), 
             suffix = c('_from','_to')) %>%
  filter(hemi_from == hemi_to) %>%
  filter(median_mm > 0) %>%
  select(from_roiidx, to_roiidx, SHORTNAME_from, SHORTNAME_to, median_mm)
```

## The shortest distance between two ROIs is 16mm

The next shortest is 21mm

```{r}

head(median_distances1 %>% arrange(median_mm))
```
## let's look at two shortest distances per RO1

```{r}
shortest_distances <- median_distances1 %>%
  group_by(from_roiidx) %>%
  top_n(-2, median_mm)
  
summary(shortest_distances$median_mm)
```
## histogram of 2 shortest distances per ROI

note that the median really stands out

```{r}
shortest_distances %>%
  ggplot(aes(x = median_mm)) + geom_histogram(binwidth = 2) 
```

## build a table of percent shortest distances

```{r}
short_threshold = 70
the_short_list <- median_distances1 %>%
  mutate(from_NET = str_sub(SHORTNAME_from, 1,2),
         to_NET = str_sub(SHORTNAME_to, 1,2)) %>%
  mutate(wth_between = if_else(from_NET == to_NET, "within", "between"),
         shorter = if_else(median_mm < short_threshold, "short", "long")) 
 
the_short_list %>%  
   group_by(from_NET, wth_between, shorter) %>%
  summarise(n = n()) %>%
  mutate(n = n/2) %>%
  spread(shorter, n) %>%
  mutate(ratio = short/(short + long)) %>%
  knitr::kable()
```
## a histogram of all the measured distances

```{r}
median_distances1 %>%
  ggplot(aes(x = median_mm)) + geom_histogram()
```

## write the list of shortest edges out to a csv

```{r}
the_short_list %>%
  filter(shorter == "short") %>%
  write_csv(path = "../tmp_Rdata/Yeo80_shorter_edges.csv")
```

