---
title: "ABIDEI PINT FC mat stats"
author: "Erin W Dickie"
date: "November 04, 2016"
output: html_document
---

This is an analysis of the FC Mats

```{r}

library(dplyr)
library(tidyr)
library(broom)
library(igraph)
library(corrplot)
library(effsize)
library(knitr)
library(ggplot2)

## set pint_dir to be the pint outputs
pint_dir <- "../20161101_PINT_Yeo7-80v_rad6-6-12/"
## set file type to reference the tvertex meants..
pint_pattern = "Yeo7-80v_rad6-6-12"
## reading in the qced_sublists csv to get the sublists
qced_sublists <- read.csv("../phenotypic_and_qc/ABIDEI_Pheno_QCed_20161010.csv")
##load the more descriptive list of the vertices...not we may want to change a few based on these results...
Yeo7_80verts<- read.csv("../phenotypic_and_qc/Yeo7_80verts_201610.csv")
```

## Some functions were using

```{r}
## functions
read_subject_meants <- function(subid, pint_dir, pint_pattern, vertex_type) {
  meants <- read.csv(file.path(pint_dir,
                               subid,
                               paste(subid, pint_pattern, vertex_type, "meants.csv", sep="_")),
                     header=FALSE)
  return(meants)
}

load_all_subject_matrices <- function(subids, myedgenames, pint_dir, 
                                      pint_pattern, vertex_type) {
  ## use these parameters to set up a black dataframe to hold all the correlations
  theZs <- as.data.frame(cbind("subid" = subids, 
                               matrix(numeric(), 
                                      nrow = length(subids),
                                      ncol = numedges,
                                      dimnames = list(1:length(subids),
                                                      myedgenames))))
  theZs[ ,2:ncol(theZs)] <- numeric(nrow(theZs)*(ncol(theZs)-1))
  
  ## now correlate everyones ts and write the correlations to a massive data frame
  for (i in 1:nrow(theZs)) {
    ## get the subid from the dataframe and read in the meants
    meants <- read_subject_meants(theZs$subid[i], pint_dir, pint_pattern, vertex_type)
    
    ## correlate and graph
    cormat <- cor(t(meants))
    g<-graph_from_adjacency_matrix(cormat,mode="upper", 
                                   weighted=T, diag=F)
    # take the egde list as a vector
    thecorrs <- E(g)$weight
    
    # apply the Z transform (so we can do stats)
    theseZ <- atanh(thecorrs)
    
    # save the output to the data.frame
    theZs[i,2:ncol(theZs)] <- theseZ
  }
  return(theZs)
}

make_corrplot <-function(data, attrtoplot, graph_title) {
  forplot <- as.data.frame(data) 
  forplot <- forplot %>% separate(Edge, into = c("V1", "V2"), sep = '\\.')
  g <- graph_from_data_frame(forplot, directed=F)
  g.mat <- get.adjacency(g, attr=attrtoplot)
  corrplot(as.matrix(g.mat), 
           is.corr = F, tl.cex = 0.3, tl.col = "black",
           title = graph_title)
}

draw_circle_graph <- function(data, Yeo7_80verts) {
CircleOrder <- as.character(select(arrange(Yeo7_80verts, CircleOrder1),SHORTNAME)$SHORTNAME)
CircleOrder <- c(CircleOrder[65:length(CircleOrder)], CircleOrder[1:64])
g <- graph.data.frame(data, directed = F)
Yeo7_80verts$wnetname <- substr(Yeo7_80verts$SHORTNAME,3,6)
Yeo7_80verts$Vhex <- NA
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==2] <- "#781286"
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==3] <- "#00760E"
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==4] <- "#C43AFA"
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==5] <- "#4682B4"
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==6] <- "#E69422"
Yeo7_80verts$Vhex[Yeo7_80verts$NETWORK==7] <- "#CD3E3A"
V(g)$color <- Yeo7_80verts$Vhex
g$layout <- layout_in_circle(g,order = CircleOrder)
d <-as.matrix(dist(g$layout, method = "euclidean", 
          upper = TRUE, diag = TRUE, p = 2))
gdist <-graph_from_adjacency_matrix(as.matrix(dist(g$layout, method = "euclidean", 
                                          upper = TRUE, diag = TRUE, p = 2)),
                                     mode="upper", weighted=T, diag=F)
dist.df <- cbind(data,E(gdist)$weight)
dist.df$scaledD <- (dist.df$`E(gdist)$weight`/(-2)) + 1
dist.df$scaleDc <- dist.df$scaledD
dist.df$netcombo <- paste(substr(dist.df$V1,1,2), substr(dist.df$V2,1,2),sep =".")
switchlist <- c("DA.DM","DM.EX", "DM.SA", "EX.SA", "SA.SM", "SA.VI")
dist.df$scaleDc[dist.df$netcombo %in% switchlist] <- dist.df$scaleD[dist.df$netcombo %in% switchlist] * (-1)

plot(g, edge.width=E(g)$logp*5, 
     edge.curved=dist.df$scaleDc, 
     edge.color = E(g)$posneg,
     vertex.size = 7,
     vertex.label.cex = 0.6,
     vertex.label = Yeo7_80verts$wnetname,
     vertex.label.color = "black")
}
```

### Load the ivertex and tvertex Zs for all subjects

```{r}
## read in one examplar to get some parameters we want
meants1 <- read_subject_meants(qced_sublists$subid[1], pint_dir, pint_pattern, 'ivertex')
## sort the meants into the order from the new naming scheme
names(meants1) <- paste0("TR_",1:ncol(meants1))
Yeo_meants <- cbind(Yeo7_80verts,meants1)

## correlate and graph from the first one
cormat <- cor(t(select(Yeo_meants,starts_with("TR_"))))
rownames(cormat) <- Yeo_meants$SHORTNAME
colnames(cormat) <- Yeo_meants$SHORTNAME
g<-graph_from_adjacency_matrix(cormat,mode="upper", 
                               weighted=T, diag=F, 
                               add.rownames = "code")
g.df <- as.data.frame(get.edgelist(g), names=T)

## get two variables of interest.. edgenames and the number of edges
myedgenames <- paste(g.df[ ,1],g.df[ ,2],sep=".") ## the V1.V2 name
numedges <- length(myedgenames)                   ## the number of edges


## get all the Z's from ivertex
theZs_ivertex <- load_all_subject_matrices(as.character(qced_sublists$subid),
                                           myedgenames, pint_dir, 
                                           pint_pattern, 'ivertex')

## merge all those correlations back with the demographics
alldemZs_i_m <- merge(qced_sublists,theZs_ivertex,by="subid") %>%
    gather_("Edge", "FC", myedgenames)


## get all the Z's from tvertex
theZs_tvertex <- load_all_subject_matrices(as.character(qced_sublists$subid),
                                           myedgenames, pint_dir, 
                                           pint_pattern, 'tvertex')

## merge all those correlations back with the demographics
alldemZs_t_m <- merge(qced_sublists,theZs_tvertex,by="subid")  %>%
  gather_("Edge", "FC", myedgenames)  

## rm the extra object in memory
rm(cormat, meants1, theZs_tvertex, theZs_ivertex, Yeo_meants, g)
```

### Run the ivertex and tvertex models for all subjects

```{r}
lmres_i <- alldemZs_i_m %>%
  group_by(Edge) %>%
  do(tidy(lm(FC ~ DX_GROUP + AGE_AT_SCAN + site + Comp.1 + Comp.2,.)))

lmres_i_Dx <- lmres_i %>% 
  filter(term == "DX_GROUPTD") 

lmres_t <- alldemZs_t_m %>%
  group_by(Edge) %>%
  do(tidy(lm(FC ~ DX_GROUP + AGE_AT_SCAN + site + Comp.1 + Comp.2,.)))

lmres_t_Dx <- lmres_t %>% 
  filter(term == "DX_GROUPTD") 

DxResults <- merge(lmres_i_Dx, lmres_t_Dx, by = "Edge", suffixes = c(".i",".t")) %>%
  select(Edge, statistic.i, p.value.i, statistic.t, p.value.t)

DxResults$p.FDR.i <- p.adjust(DxResults$p.value.i, method = "fdr")
DxResults$p.FDR.t <- p.adjust(DxResults$p.value.t, method = "fdr")

DxResults <- DxResults %>%
  select(Edge, statistic.t, p.value.t, p.FDR.t, statistic.i, p.value.i, p.FDR.i) 
  
  
write.csv(separate(DxResults, Edge, into = c("V1","V2"), sep = "\\."),file.path(pint_dir,"FCmat_DX_Results.csv"), row.names = F)

rm(lmres_t_Dx, lmres_i_Dx)
```

## Report on ivertex results - whole sample

```{r fig.height=6, fig.width=12}
par(mfrow=c(1,2))
make_corrplot(DxResults, "statistic.t" , "tvertex TD>ASD t-stat")
make_corrplot(DxResults, "statistic.i" , "ivertex TD>ASD t-stat")
```

```{r fig.height=8, fig.width=8}
forgraph_i <- DxResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('.i'))
forgraph_i$posneg <- ifelse(forgraph_i$statistic.i < 0 , 1, 2)
forgraph_i$logp <- log(forgraph_i$p.value.i)
forgraph_i$statistic.i[forgraph_i$p.FDR.i > 0.05] <- 0
forgraph_i$logp[forgraph_i$p.FDR.i > 0.05] <- 0
forgraph_i$posneg[forgraph_i$p.FDR.i > 0.05] <- NA

report <- forgraph_i %>% 
  mutate("net1" = substr(V1,1,2),
         "net2" = substr(V2,1,2)) %>%
  filter(p.FDR.i < 0.05, statistic.i > 0) %>%
  group_by(net1,net2) %>%
  summarise(n=n())

draw_circle_graph(forgraph_i, Yeo7_80verts)
```

## Breakdown of ivertex, FDR corrected hypo-connectivity by network

```{r}
greport <- graph.data.frame(report, directed = F)
report_mat <- as.matrix(get.adjacency(greport, type = "lower", attr = "n"))
kable(report_mat)
```

```{r fig.height=8, fig.width=8}
forgraph_t <- DxResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('.t'))
forgraph_t$posneg <- ifelse(forgraph_t$statistic.t < 0 , 1, 2)
forgraph_t$logp <- log(forgraph_t$p.value.t)
forgraph_t$statistic.t[forgraph_t$p.FDR.t > 0.05] <- 0
forgraph_t$logp[forgraph_t$p.FDR.t > 0.05] <- 0
forgraph_t$posneg[forgraph_t$p.FDR.t > 0.05] <- NA

report <- forgraph_t %>% 
  mutate("net1" = substr(V1,1,2),
         "net2" = substr(V2,1,2)) %>%
  filter(p.FDR.t < 0.05, statistic.t > 0) %>%
  group_by(net1,net2) %>%
  summarise(n=n())


draw_circle_graph(forgraph_t, Yeo7_80verts)
```

## Breakdown of tvertex, FDR corrected hypo-connectivity by network

```{r}
greport <- graph.data.frame(report, directed = F)
report_mat <- as.matrix(get.adjacency(greport, type = "lower", attr = "n"))
kable(report_mat)
```

```{r}
DxResults$sigfor <- NA
DxResults$sigfor[DxResults$p.FDR.t < 0.05 & DxResults$p.FDR.i < 0.05] <- "both"
DxResults$sigfor[DxResults$p.FDR.t < 0.05 & DxResults$p.FDR.i > 0.05] <- "tvertex"
DxResults$sigfor[DxResults$p.FDR.t > 0.05 & DxResults$p.FDR.i < 0.05] <- "ivertex"

dx_report <- DxResults %>%
  rowwise() %>%
  mutate("Minp" = min(p.FDR.i,p.FDR.t)) %>%
  filter(Minp < 0.05)

dx_report$sigfor <- NA
dx_report$sigfor[dx_report$p.FDR.t < 0.05 & dx_report$p.FDR.i < 0.05] <- "both"
dx_report$sigfor[dx_report$p.FDR.t < 0.05 & dx_report$p.FDR.i > 0.05] <- "tvertex"
dx_report$sigfor[dx_report$p.FDR.t > 0.05 & dx_report$p.FDR.i < 0.05] <- "ivertex"


Num_tvertex_DxResults <- nrow(filter(dx_report, p.FDR.t < 0.05))
Num_ivertex_DxResults <- nrow(filter(dx_report, p.FDR.i < 0.05))
kable(filter(dx_report, sigfor == "both"), caption = paste("Effects of DX (total sample), Number for both ", nrow(filter(dx_report, sigfor == "both"))))
                                                           
kable(filter(dx_report, sigfor == "ivertex"), caption = paste("Effects of DX (total sample), Number for ivertex but not tvertex", nrow(filter(dx_report, sigfor == "ivertex"))))

kable(filter(dx_report, sigfor == "tvertex"), caption = paste("Effects of DX (total sample), Number for ivertex but not tvertex", nrow(filter(dx_report, sigfor == "tvertex"))))

dx_delta <- DxResults %>%
  mutate("abstdiff" = abs(statistic.t) - abs(statistic.i),
         "nets" = paste(substr(Edge, 1,2), substr(Edge,7,8),sep = ".")) 

kable((dx_delta  %>% group_by(nets) %>%
    summarise("mean_tdiff" = mean(abstdiff)) %>%
    arrange(mean_tdiff)))

ggplot(dx_delta, aes(x=statistic.i, y=statistic.t)) +
  geom_point(aes(color = sigfor)) + geom_smooth(method = "lm") + 
  geom_abline(intercept = 0, slope = 1) +
  facet_wrap(~nets)

ggplot(dx_delta, aes(x=statistic.i, y=statistic.t)) +
  geom_point(aes(color = sigfor)) + geom_smooth(method = "lm") + 
  geom_abline(intercept = 0, slope = 1) 
```

# Now How about the age groups
```{r}
ilmres_agegroups <- alldemZs_i_m %>%
  filter(!is.na(AGE_GROUP)) %>%
  group_by(Edge, AGE_GROUP) %>%
  do(tidy(lm(FC ~ DX_GROUP + AGE_AT_SCAN + site + Comp.1 + Comp.2,.)))

ilmres_Dx_agegroups <- ilmres_agegroups %>% 
  filter(term == "DX_GROUPTD") %>%
  select(Edge, starts_with("statistic"), starts_with("p.value")) %>%
  gather(Measure, Value, -Edge, -AGE_GROUP)

ilmres_Dx_agegroups <- as.data.frame(ilmres_Dx_agegroups) %>%
  unite(NewCol, Measure, AGE_GROUP) %>%
  select(Edge, NewCol, Value) %>%
  spread(NewCol, Value) 

tlmres_agegroups <- alldemZs_t_m %>%
  filter(!is.na(AGE_GROUP)) %>%
  group_by(Edge, AGE_GROUP) %>%
  do(tidy(lm(FC ~ DX_GROUP + AGE_AT_SCAN + site + Comp.1 + Comp.2,.)))

tlmres_Dx_agegroups <- tlmres_agegroups %>% 
  filter(term == "DX_GROUPTD") %>%
  select(Edge, starts_with("statistic"), starts_with("p.value")) %>%
  gather(Measure, Value, -Edge, -AGE_GROUP)

tlmres_Dx_agegroups <- as.data.frame(tlmres_Dx_agegroups) %>%
  unite(NewCol, Measure, AGE_GROUP) %>%
  select(Edge, NewCol, Value) %>%
  spread(NewCol, Value) 

DxAgeResults <- merge(ilmres_Dx_agegroups, tlmres_Dx_agegroups, by = "Edge", suffixes = c(".i",".t"))

DxAgeResults$p.FDR_Child.i <- p.adjust(DxAgeResults$p.value_Child.i, method = "fdr")
DxAgeResults$p.FDR_Child.t <- p.adjust(DxAgeResults$p.value_Child.t, method = "fdr")
DxAgeResults$p.FDR_ADO.i <- p.adjust(DxAgeResults$p.value_ADO.i, method = "fdr")
DxAgeResults$p.FDR_ADO.t <- p.adjust(DxAgeResults$p.value_ADO.t, method = "fdr")
DxAgeResults$p.FDR_Adult.i <- p.adjust(DxAgeResults$p.value_Adult.i, method = "fdr")
DxAgeResults$p.FDR_Adult.t <- p.adjust(DxAgeResults$p.value_Adult.t, method = "fdr")

 
DxAgeResults <- DxAgeResults %>%
   select(Edge, 
          statistic_Child.t, statistic_ADO.t, statistic_Adult.t,
          p.value_Child.t, p.value_ADO.t, p.value_Adult.t,
          p.FDR_Child.t, p.FDR_ADO.t, p.FDR_Adult.t,
          statistic_Child.i, statistic_ADO.i, statistic_Adult.i,
          p.value_Child.i, p.value_ADO.i, p.value_Adult.i,
          p.FDR_Child.i, p.FDR_ADO.i, p.FDR_Adult.i) 

write.csv(separate(DxAgeResults, Edge, into = c("V1","V2"), sep = "\\."),file.path(pint_dir,"FCmat_DXxAgeGroups_Results.csv"), row.names = F)

rm(tlmres_Dx_agegroups, ilmres_Dx_agegroups)
```

## plot all the Age Corrplots

```{r fig.height=20, fig.width=12}
par(mfrow=c(3,2))
make_corrplot(DxAgeResults, "statistic_Child.t", "tvertex TD>ASD t-stat Children")
make_corrplot(DxAgeResults, "statistic_Child.i", "ivertex TD>ASD t-stat Children")
make_corrplot(DxAgeResults, "statistic_ADO.t", "tvertex TD>ASD t-stat Adolescent")
make_corrplot(DxAgeResults, "statistic_ADO.i", "ivertex TD>ASD t-stat Adolescent")
make_corrplot(DxAgeResults, "statistic_Adult.t", "tvertex TD>ASD t-stat Adult")
make_corrplot(DxAgeResults, "statistic_Adult.i", "ivertex TD>ASD t-stat Adult")
```

```{r fig.height=8, fig.width=8}
fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_Child.i'))
fgraph$posneg <- ifelse(fgraph$statistic_Child.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_Child.i)
fgraph$statistic_Child.i[fgraph$p.FDR_Child.i > 0.05] <- 0
fgraph$logp[fgraph$p.FDR_Child.i > 0.05] <- 0
fgraph$posneg[fgraph$p.FDR_Child.i > 0.05] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_ADO.i'))
fgraph$posneg <- ifelse(fgraph$statistic_ADO.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_ADO.i)
fgraph$statistic_ADO.i[fgraph$p.FDR_ADO.i > 0.05] <- 0
fgraph$logp[fgraph$p.FDR_ADO.i > 0.05] <- 0
fgraph$posneg[fgraph$p.FDR_ADO.i > 0.05] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_Adult.i'))
fgraph$posneg <- ifelse(fgraph$statistic_Adult.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_Adult.i)
fgraph$statistic_Adult.i[fgraph$p.FDR_Adult.i > 0.05] <- 0
fgraph$logp[fgraph$p.FDR_Adult.i > 0.05] <- 0
fgraph$posneg[fgraph$p.FDR_Adult.i > 0.05] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_Child.i'))
fgraph$posneg <- ifelse(fgraph$statistic_Child.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_Child.i)
fgraph$statistic_Child.i[fgraph$p.value_Child.i > 0.01] <- 0
fgraph$logp[fgraph$p.value_Child.i > 0.01] <- 0
fgraph$posneg[fgraph$p.value_Child.i > 0.01] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_ADO.i'))
fgraph$posneg <- ifelse(fgraph$statistic_ADO.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_ADO.i)
fgraph$statistic_ADO.i[fgraph$p.value_ADO.i > 0.01] <- 0
fgraph$logp[fgraph$p.pvalue_ADO.i > 0.01] <- 0
fgraph$posneg[fgraph$p.pvalue_ADO.i > 0.01] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

fgraph <- DxAgeResults %>%
  separate(Edge, into = c("V1","V2"), sep = "\\.") %>%
  select(V1, V2, ends_with('_Adult.i'))
fgraph$posneg <- ifelse(fgraph$statistic_Adult.i < 0 , 1, 2)
fgraph$logp <- log(fgraph$p.value_Adult.i)
fgraph$statistic_Adult.i[fgraph$p.value_Adult.i > 0.01] <- 0
fgraph$logp[fgraph$p.value_Adult.i > 0.01] <- 0
fgraph$posneg[fgraph$p.value_Adult.i > 0.01] <- NA
draw_circle_graph(fgraph, Yeo7_80verts)

```

```{r}
dx_agereport <- DxAgeResults %>%
  rowwise() %>%
  mutate("Minp" = min(p.FDR_Child.i,p.FDR_Child.t,
                      p.FDR_ADO.i,p.FDR_ADO.t,
                      p.FDR_Adult.i,p.FDR_Adult.t)) %>%
  filter(Minp < 0.05)

kable(dx_agereport, caption = "Effects of DX (by Age)")

```

```{r}
# lmres_ia <- alldemZs_i_m %>%
#   group_by(Edge) %>%
#   do(tidy(lm(FC ~ ADOS_TOTAL + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
#   filter(term == "ADOS_TOTAL")
# 
# lmres_ia$p.FDR = p.adjust(lmres_ia$p.value, method = "fdr")
# kable(filter(lmres_ia, p.value < 0.005))
# 
# lmres_ifiq <- alldemZs_i_m %>%
#   group_by(Edge, DX_GROUP) %>%
#   do(tidy(lm(FC ~ FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
#   filter(term == "FIQ", DX_GROUP == "ASD")
# 
# lmres_ifiq$p.FDR = p.adjust(lmres_ifiq$p.value, method = "fdr")
# kable(filter(lmres_ifiq, p.value < 0.005))
# 
# lmres_iafiq <- alldemZs_i_m %>%
#   group_by(Edge) %>%
#   do(tidy(lm(FC ~ ADOS_TOTAL + FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
#   filter(Edge %in% ) 
# 
# lmres_iafiq$p.FDR = p.adjust(lmres_iafiq$p.value, method = "fdr")
# kable(filter(lmres_iafiq, p.value < 0.005))
# 
# 
# lmres_iagfiq <- alldemZs_i_m %>%
#   group_by(Edge, AGE_GROUP) %>%
#   do(tidy(lm(FC ~ ADOS_TOTAL + FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
#   filter(term == "ADOS_TOTAL")
# 
# kable(filter(lmres_iagfiq, p.value < 0.005)  %>% arrange(AGE_GROUP))
# 
# lmres_ifiqag <- alldemZs_i_m %>%
#   group_by(Edge, AGE_GROUP) %>%
#   do(tidy(lm(FC ~ ADOS_TOTAL + FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
#   filter(term == "FIQ")
# 
# 
# kable(filter(lmres_ifiqag, p.value < 0.005)  %>% arrange(AGE_GROUP))

## short edge list
shortedgelist <- DxResults %>% 
  filter(p.FDR.i < 0.05) %>%
  select(Edge)

lmres_iafiq <- alldemZs_i_m %>%
  group_by(Edge) %>%
  do(tidy(lm(FC ~ ADOS_TOTAL + FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.))) %>%
  filter(Edge %in% as.character(shortedgelist$Edge))

kable(lmres_iafiq %>% filter(term == "ADOS_TOTAL", p.value < 0.01))
kable(lmres_iafiq %>% filter(term == "FIQ", p.value < 0.01))

## short edge list
shortedgelist_Child <- DxAgeResults %>% 
  filter(p.value_Child.i < 0.01) %>%
  select(Edge)

shortedgelist_Adult <- DxAgeResults %>% 
  filter(p.value_Adult.i < 0.01) %>%
  select(Edge)

shortedgelist_ADO <- DxAgeResults %>% 
  filter(p.value_ADO.i < 0.01) %>%
  select(Edge)

lmres_iafiq_c <- alldemZs_i_m %>%
  group_by(Edge, AGE_GROUP) %>%
  do(tidy(lm(FC ~ ADOS_TOTAL + FIQ + AGE_AT_SCAN + site + Comp.1 + Comp.2,.)))

kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_Child$Edge)) %>%
  filter(AGE_GROUP == "Child", term == "ADOS_TOTAL", p.value < 0.05))
kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_Child$Edge)) %>%
  filter(AGE_GROUP == "Child", term == "FIQ", p.value < 0.05))

kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_Adult$Edge)) %>%
  filter(AGE_GROUP == "Adult", term == "ADOS_TOTAL", p.value < 0.05))
kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_Adult$Edge)) %>%
  filter(AGE_GROUP == "Adult", term == "FIQ", p.value < 0.05))

kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_ADO$Edge)) %>%
  filter(AGE_GROUP == "ADO", term == "ADOS_TOTAL", p.value < 0.05))
kable(lmres_iafiq_c %>%
  filter(Edge %in% as.character(shortedgelist_ADO$Edge)) %>%
  filter(AGE_GROUP == "ADO", term == "FIQ", p.value < 0.05))

rm(ilmres_Dx_agegroups, lmres_iafiq_c, lmres_iafiq, ilmres_agegroups)
```

# trying the anova's with age interactions...

```{r fig.width=9, fig.height=20}

netZs_i <- alldemZs_i_m %>%
  mutate(NETS = paste(substr(Edge,1,2), substr(Edge,7,8),sep = ".")) %>%
  group_by(subid,NETS) %>%
  summarise("MeanFC" = mean(FC))

netZs_t <- alldemZs_t_m %>%
  mutate(NETS = paste(substr(Edge,1,2), substr(Edge,7,8),sep = ".")) %>%
  group_by(subid,NETS) %>%
  summarise("MeanFC" = mean(FC))

netanova_i <- merge(netZs_i, qced_sublists, by = "subid") %>%
  mutate("AGE_centered" = AGE_AT_SCAN- mean(AGE_AT_SCAN, na.rm=T)) %>%
  group_by(NETS) %>%
  do(tidy(anova(lm(MeanFC ~ DX_GROUP*AGE_centered + site + Comp.1 + Comp.2, .))))

kable(netanova_i)

netanova_ig <- merge(netZs_i, qced_sublists, by = "subid") %>%
  mutate("AGE_centered" = AGE_AT_SCAN- mean(AGE_AT_SCAN, na.rm=T)) %>%
  group_by(NETS) %>%
  do(tidy(anova(lm(MeanFC ~ DX_GROUP*AGE_GROUP + site + Comp.1 + Comp.2, .))))

kable(netanova_ig)

forplot_ngi <- merge(netZs_i, qced_sublists, by = "subid") %>%
  filter(!is.na(AGE_GROUP))

forplot_ngi$AGE_GROUP <- factor(forplot_ngi$AGE_GROUP, levels = c("Child", "ADO", "Adult"))
ggplot(merge(netZs_i, qced_sublists, by = "subid"),
       aes(x = AGE_AT_SCAN, y = MeanFC, color = DX_GROUP)) +
  geom_point(color = "black") + geom_smooth(method = "lm") +
  scale_x_log10() +
  facet_wrap(~NETS, scales = "free")

ggplot(forplot_ngi,
       aes(x = AGE_GROUP, y = MeanFC, color = DX_GROUP)) +
  geom_boxplot() +
  facet_wrap(~NETS, scales = "free", ncol = 3)  

ggplot(forplot_ngi,
       aes(x = DX_GROUP, y = MeanFC, color = AGE_GROUP)) +
  geom_boxplot() +
  facet_wrap(~NETS, scales = "free", ncol = 3) 

netanova_tg <- merge(netZs_t, qced_sublists, by = "subid") %>%
  mutate("AGE_centered" = AGE_AT_SCAN- mean(AGE_AT_SCAN, na.rm=T)) %>%
  group_by(NETS) %>%
  do(tidy(anova(lm(MeanFC ~ DX_GROUP*AGE_GROUP + site + Comp.1 + Comp.2, .))))

forplot_ngt <- merge(netZs_t, qced_sublists, by = "subid") %>%
  filter(!is.na(AGE_GROUP))

forplot_ngt$AGE_GROUP <- factor(forplot_ngt$AGE_GROUP, levels = c("Child", "ADO", "Adult"))

ggplot(forplot_ngt,
       aes(x = DX_GROUP, y = MeanFC, color = AGE_GROUP)) +
  geom_boxplot() +
  facet_wrap(~NETS, scales = "free", ncol = 3)

netanova_ig$NETS.term <- paste(netanova_ig$NETS, netanova_ig$term, sep = ".")
netanova_tg$NETS.term <- paste(netanova_tg$NETS, netanova_tg$term, sep = ".")

netanova_report <- merge(netanova_tg, netanova_ig, by = "NETS.term", suffixes = c('.t','.i')) %>%
  select(NETS.term, df.t, statistic.t, p.value.t, statistic.i, p.value.i)

kable(netanova_report)
```