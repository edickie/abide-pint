---
title: "FC matrix related results"
output:
  pdf_document: 
    latex_engine: xelatex
  html_notebook: default
  html_document: default
---

# ABIDEI results reported in paper

Runs all FC matrix related results for ABIDEI dataset

```{r}
library(here)
library(igraph)
library(tidyverse)
library(broom)
```

## source these helper scripts from the R package

```{r}
source(here('R','load_data.R'))
source(here('R','reporting_helpers.R'))
```

## read the pheno data and define the paths we need for today

```{r}

# define the pint dir
pint_dir <- here('data','20161101_PINT_Yeo7-80v_rad6-6-12')
#pint_dir <- here('data','20180208_PINT_fullcorr_Yeo7-80v_rad6-6-12')
# pint_dir <- here('data','20180206_PINT_Yeo7-80v_rad8-6-12')
pvertex_col <- 'ivertex' # the pvertices are referred to as ivertices

# ana_prefix <- "fullcorr"
ana_prefix <- "pcorr_paper"

# read in the Yeo80 template
Yeo80 <- load_Yeo_80verts()

## converts the NET variable to a ordered factor
parse_NET_for_table <- function(data) {
  result <- data %>%
    mutate(NET = parse_factor(NET, 
                              levels = c("SM", "VI", "DM", "FP", "DA", "VA", "Total"),
                              ordered = TRUE))
  return(result)
}

# read in the abideI pheno and QC data
pheno <- read_mangle_qced_pheno()

# define the pint outputs all outputs folder paths where with a subid in qced
tvertex_key <- define_meants_df(pint_dir, 'tvertex') %>%
  separate(meants_file, into = c('subid'), 
           sep = .Platform$file.sep, remove = F, extra = "drop") %>%
  semi_join(pheno, by = "subid")
```

# read in all the meants files

```{r}
## find the personalized meants paths by find and replace tvertex with 'ivertex' or 'pvertex'
pvertex_key <- tvertex_key %>%
  mutate(meants_file = str_replace(meants_file, 'tvertex', pvertex_col))

## combine the template and personalized paths into one file key
allmeants_key <- bind_rows("Template" = tvertex_key,
                       "Personalized" = pvertex_key,
                       .id = "vertex_type")


Zcors_full <- allmeants_key %>%
  mutate(thispath = file.path(pint_dir, meants_file)) %>%
  mutate(cvt = map(thispath, ~meants_file_to_corrZ(.x, 
                                              g_mode = "directed",
                                              g_diag = FALSE,
                                              roi.names = Yeo80$SHORTNAME))) %>%
  select(subid, vertex_type, cvt) %>%
  unnest(cvt)
```


## calculate the summary's of within and between subject connectivity

```{r}
Zcors_full  <- Zcors_full %>%
  ungroup() %>%
  separate(Edge, into = c("to","from"), sep = "_") %>%
    mutate(Net1 = substr(to, 1,2),
           Net2 = substr(from, 1,2),
           wtbw = ifelse(Net1==Net2, "Within", "Between")) 
```

```{r}
#' calculate by subject within and between network connectivity
#' 
#' requires input dataframe with columns 
#' "Net1" "subid", 'wtbw', 'vertex_type' and "Zcor"
#' @param Zcor_full_data dataframe with inputs columns 
#' @returns dataframe with within bewtween means by subid, vertex_type, and network
calc_subject_wtbw <- function(Zcor_full_data) {
  # calculate by network
  wtbw_bysub_net <- Zcor_full_data %>%
    ungroup() %>%
    group_by(Net1, subid, wtbw, vertex_type) %>%
    summarise(sMeanZ = mean(Zcor))
  
  # calculate by total
  wtbw_bysub_total <- Zcor_full_data %>%
    ungroup() %>%
    group_by(subid, wtbw, vertex_type) %>%
    summarise(sMeanZ = mean(Zcor)) %>%
    mutate(Net1 = "Total")
  
  # bind the two into one df
  wtbw_bysub <- bind_rows(wtbw_bysub_net, wtbw_bysub_total)
  
  return(wtbw_bysub)
}

#' builds the supplemental table data
build_wtbw_table <- function(wtbw_by_subject) {
wtbn_net_table <- wtbw_by_subject %>%
  ungroup() %>%
  spread(vertex_type, sMeanZ) %>%
  mutate(Zchange = Personalized - Template) %>%
  gather(vertex_type, sMeanZ, Personalized, Template, Zchange) %>%
  group_by(Net1, wtbw, vertex_type) %>%
  summarise(n = n(),
            MeanZ = mean(sMeanZ),
            sdZ = sd(sMeanZ),
            msd = sprintf('%3.2f(%3.2f)',MeanZ, sdZ)) %>%
  ungroup() %>%
  select(Net1, n, wtbw, vertex_type, msd) %>%
  spread(vertex_type, msd) %>%
  rename(NET = Net1) %>%
  parse_NET_for_table() %>%
  arrange(wtbw, NET)
}
```

```{r}
all_wtbn_table1 <- Zcors_full %>%
  calc_subject_wtbw() %>%
  build_wtbw_table() 

knitr::kable(all_wtbn_table1,
             caption = "Within between strengths, all connections")
```

### Now get the within and between network stengths excluding long range connections

shorter edges were defined as those less than 70mm apart in reports/
```{r}
Yeo80_shorter_edges <- read_csv(here('tmp_Rdata','Yeo80_shorter_edges.csv'),
                                col_types = c(
                                  from_roiidx = col_integer(),
                                  to_roiidx = col_integer(),
                                  SHORTNAME_from = col_character(),
                                  SHORTNAME_to = col_character(),
                                  median_mm = col_double(),
                                  from_NET = col_character(),
                                  to_NET = col_character(),
                                  wth_between = col_character(),
                                  shorter = col_character()
                                )) %>%
  select(SHORTNAME_from, SHORTNAME_to, median_mm)

all_wtbn_table2 <- Zcors_full %>%
  anti_join(Yeo80_shorter_edges, by = c('from' = 'SHORTNAME_from', 
                                        'to' = 'SHORTNAME_to')) %>%
  calc_subject_wtbw() %>%
  build_wtbw_table()

knitr::kable(all_wtbn_table2,
             caption = "Within between strengths, only long range connections")
```
```{r eval = F}
bind_rows('All_Edges' = all_wtbn_table1, 
          'LongRange' = all_wtbn_table2,
          .id = "Edges_included") %>%
  write_csv(path = here('plots','suppl_table_wtbn_strengths.csv'))
```


# Running the linear models

```{r}
## for the stats, we only want the upper triangle
Zcors_upper <- allmeants_key %>%
  mutate(thispath = file.path(pint_dir, meants_file),
    cvt = map(thispath, ~meants_file_to_corrZ(.x, 
                                              g_mode = "upper",
                                              g_diag = FALSE,
                                              roi.names = Yeo80$SHORTNAME))) %>%
  select(subid, cvt, vertex_type) %>%
  unnest(cvt)
```


```{r}
FC_model_DX <- function(df) {
  lm(Zcor ~ DX_GROUP + Age_Centered + SEX + FIQ_Centered + site + Comp.1 + Comp.2, data = df)
}

theZs_formodels <- Zcors_upper %>%
  inner_join(pheno, by="subid") %>%
  ungroup() %>%
  select(Zcor, Edge, subid, DX_GROUP, Age_Centered, 
         SEX, FIQ_Centered, site, Comp.1, Comp.2,
         mean_fd, dvars, vertex_type) %>%
  mutate(mean_fd_log10 = log10(mean_fd))
```


## run the main FC linear model

```{r}
lmres_fc_glance <- theZs_formodels %>%
  group_by(Edge, vertex_type) %>%
  do(glance(FC_model_DX(.))) %>%
  select(Edge, vertex_type, `df.residual`) 

lmres_fc <- theZs_formodels %>%
  group_by(Edge, vertex_type) %>%
  do(tidy(FC_model_DX(.))) %>%
  ungroup() %>%
  inner_join(lmres_fc_glance, by = c("Edge", "vertex_type")) %>%
  group_by(vertex_type, term) %>%
  mutate(p_FDR = p.adjust(`p.value`, method = "fdr")) %>%
  ungroup() %>%
  group_by(term) %>%
  mutate(p_FDRb = p.adjust(`p.value`, method = "fdr"))
```
```{r}
lmres_fc %>%
  filter(term == "DX_GROUPTD") %>%
  select(Edge, vertex_type, statistic) %>%
  spread(vertex_type, statistic) %>%
  mutate(net1 = str_sub(Edge, 1, 2),
         net2 = str_sub(Edge, 7, 8)) %>%
 ggplot(aes(x = Personalized, y = Template)) +
 geom_point() +
 geom_abline(slope = 1, intercept = 0, color = "red")

# ggplot(lmres_fc_net, aes(x=Template, y=Personalized)) +
#   geom_rect(xmin=-1, xmax=6.5, ymin=mint_ivertex, ymax=5.8, 
#             fill = "grey90", alpha = 0.01) +
#   geom_rect(xmin=mint_tvertex, xmax=6.5, ymax=6, ymin=-1, 
#             fill = "grey70", alpha = 0.01) +
#   geom_rect(xmin=mint_tvertex, xmax=6, ymin=mint_ivertex, ymax=6, 
#             fill =  "grey", alpha=0.02) +
#   geom_point(colour = "black", alpha = 0.1, size = 2.5) +
#   geom_point(aes(color = net1), size = 2.5) + 
#   geom_point(aes(color = net2), size = 1) +
#   scale_colour_manual(values = c("VI"="#781286", "DA"="#00760E", "VA" = "#C43AFA", "SM"="#4682B4", "FP"="#E69422", "DM"="#CD3E3A")) +
#   geom_abline(intercept = 0, slope = 1) +
#   theme_bw() +
#   theme(panel.grid.major = element_line(colour = "grey40"),
#         panel.grid.minor = element_line(colour = "grey50")) +
#   labs(x="t-statistic for ASD Diagnosis - template ROIs", 
#        y="i-statistic for ASD Diagonsis - Personalized ROIs") +
#   scale_x_continuous(limits = c(0,NA)) +
#   scale_y_continuous(limits = c(0,NA))

#ggsave('../plots/fig4_ivst_compare.png',width = 6, height = 5)
```


```{r}
lmres_fc %>%
  filter(term %in% c("DX_GROUPTD")) %>%
  ungroup() %>%
  group_by(vertex_type, term) %>%
  summarise(num_sig_pos = sum(p_FDR < 0.05 & statistic < 0),
            num_sig_neg = sum(p_FDR < 0.05 & statistic > 0)) %>%
  knitr::kable(caption = "Number of Edges significant (FDR) corrected after PINT")
```
```{r}
lmres_fc %>%
  filter(term %in% c("Comp.1", "Comp.2")) %>%
  ungroup() %>%
  group_by(vertex_type, term) %>%
  summarise(num_sig_pos = sum(p_FDR < 0.05 & statistic < 0),
            num_sig_neg = sum(p_FDR < 0.05 & statistic > 0)) %>%
  knitr::kable(caption = "Number of Edges significant (FDR) corrected after PINT")
```


```{r}
FC_model_DX_motion <- function(df) {
  lm(Zcor ~ Age_Centered + SEX + FIQ_Centered + site + mean_fd_log10 + dvars, data = df)
}

lmres_fc_glance <- theZs_formodels %>%
  group_by(Edge, vertex_type) %>%
  do(glance(FC_model_DX_motion(.))) %>%
  select(Edge, vertex_type, `df.residual`) 

lmres_fc_motion <- theZs_formodels %>%
  group_by(Edge, vertex_type) %>%
  do(tidy(FC_model_DX_motion(.))) %>%
  ungroup() %>%
  inner_join(lmres_fc_glance, by = c("Edge", "vertex_type")) %>%
  group_by(vertex_type, term) %>%
  mutate(p_FDR = p.adjust(`p.value`, method = "fdr")) %>%
  ungroup() %>%
  group_by(term) %>%
  mutate(p_FDRb = p.adjust(`p.value`, method = "fdr"))

lmres_fc_motion %>%
  filter(term %in% c("mean_fd_pT", "dvars_pT")) %>%
  ungroup() %>%
  group_by(vertex_type, term) %>%
  summarise(num_sig_pos = sum(p_FDR < 0.05 & statistic < 0),
            num_sig_neg = sum(p_FDR < 0.05 & statistic > 0)) %>%
  knitr::kable(caption = "Number of Edges significant (FDR) corrected after PINT")
```
```{r}
FC_model_pre_motion <- function(df) {
  lm(Zcor ~ Age_Centered + SEX + site, data = df)
}

 
  
reaument <- theZs_formodels %>%
  group_by(Edge, vertex_type) %>%
  do(mod = lm(Zcor ~ Age_Centered + SEX + site, .), original = (.)) %>%
  augment(mod, data = original)


```
```{r}
reaument %>%
  ungroup() %>% 
  group_by(Edge, vertex_type) %>%
  summarise(mot_cor = cor(.resid, log10(mean_fd))) %>%
  ungroup() %>% group_by(vertex_type) %>%
  summarise(median_cor = median(mot_cor))
```
```{r}
reaument %>%
  ungroup() %>% 
  group_by(Edge, vertex_type) %>%
  summarise(mot_cor = cor(.resid, log10(mean_fd))) %>%
  ungroup() %>%
  do(tidy(t.test(mot_cor ~ vertex_type, .))) 
```

```{r}
reaument %>%
  ungroup() %>% 
  group_by(Edge, vertex_type) %>%
  summarise(mot_cor = cor(.resid, log10(mean_fd))) %>%
  ggplot(aes(x = mot_cor, fill = vertex_type)) + geom_density(alpha = 0.5)

reaument %>%
  ungroup() %>% 
  group_by(Edge, vertex_type) %>%
  summarise(mot_cor = cor(.resid, log10(mean_fd))) %>%
  write_csv(path = here('tmp_Rdata', str_c(ana_prefix, '_edgewise_motion_corrs.csv')))
```






# the tvertex distances bit

### read in the distances

now we are using the standand distances sheet created by ciftify_postPINT1_concat (with S900 surfaces used as the model)

```{r}
## read the distances and fix the subid column
all_distances <- read_csv(file.path(pint_dir,"abideI_postPINT_concat_S900surfs.csv"),
                        col_types = c(
                          subid = col_character(),
                          hemi = col_character(),
                          NETWORK = col_integer(),
                          roiidx = col_integer(),
                          tvertex = col_integer(),
                          ivertex = col_integer(),
                          dist_49 = col_double(),
                          vertex_48 = col_integer(),
                          std_distance = col_double()
                        )) %>%
  separate(subid, into = c('site','sid'), extra = 'drop', sep = '_') %>%
  unite(subid, site, sid, sep = '_') 
```

```{r}
pheno_w_surface <- read_concat_surf_area(FSdir = here("data", "FSout")) %>%
  inner_join(pheno, by = "subid") %>%
  ungroup() %>%
  mutate("TotalSurf_Centered" = (TotalSurf - mean(TotalSurf))/sd(TotalSurf))
```

### for reasons that my only be apparent at the end, we are also merging in the scan info

```{r}
pheno_w_surface <- pheno_w_surface %>%
  mutate(SITE = recode(site,
                       "MaxMun" = "MAX_MUN",
                       "Caltech" = "CALTECH",
                       "Leuven" = "LEUVEN",
                       "Pitt" = "PITT",
                       "Olin" = "OLIN",
                       "Yale" = "YALE",
                       "Trinity" = "TRINITY")
  )

scan_info <- read_csv(here("phenotypic_and_qc", "ABIDEI_scanparams.csv"),
                      col_types  = c(
  Site = col_character(),
  n_anat = col_integer(),
  numTR = col_integer(),
  TRms = col_integer(),
  Scanner = col_character(),
  ScanMan = col_character()
)) %>%
  mutate(SITE = recode(Site,
                       "LEUVEN_1" =  "LEUVEN",
                       "LEUVEN_2" = "LEUVEN",
                       "UCLA_1" = "UCLA",
                       "UCLA_2" = "UCLA",
                       "UM_1" = "UM",
                       "UM_1" = "UM")) %>%
  select(numTR, TRms, Scanner, SITE, ScanMan)

scan_info <- scan_info[!duplicated(scan_info),]

pheno_w_scan_info <- inner_join(pheno_w_surface, scan_info, by = "SITE")
```

## calc network and total mean distances

```{r}
## calc networkwise distances
nettdist <- all_distances %>%
  inner_join(Yeo80, by = "roiidx") %>%
  mutate(NET = str_sub(SHORTNAME, 1, 2)) %>%
  group_by(subid, NET) %>%
  summarise(Mean_mm = mean(std_distance)) %>%
  ungroup()
  
## calc total distance and concatenate it with the Networks
mnet_tdist <- all_distances %>%
  group_by(subid) %>%
  summarise(Mean_mm = mean(std_distance)) %>%
  mutate(NET = "Total") %>%
  select(subid, NET, Mean_mm) %>%
  bind_rows(nettdist) %>%
  ungroup() %>%
  inner_join(pheno_w_scan_info, by = "subid")
```

## Run the model reported in table 1

```{r}
tdist_model_DXxAge <- function(df) {
  lm(Mean_mm ~ DX_GROUP*Age_Centered + SEX + FIQ_Centered + TotalSurf_Centered +  site + Comp.1 + Comp.2, data = df)
}

## calculate total model statistics (included the Residual SS)
neteucldist_glance <- mnet_tdist %>%
  filter(NET == "Total") %>%
  do(glance(tdist_model_DXxAge(.)))

## run the full model under 30
neteucldist_lm <- mnet_tdist %>%
  filter(AGE_AT_SCAN < 31) %>%
  group_by(NET) %>%
  do(tidy(tdist_model_DXxAge(.))) %>%
  ungroup() %>%
  mutate(df_residual = neteucldist_glance$df.residual[1])

## print the Network results, FDR corrected for the Age Effect
neteucldist_lm_totalline <- neteucldist_lm %>%
  filter(NET == "Total") %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered")) %>%
  mutate(t_str_sig = report_tstat(statistic, df_residual, p.value)) %>%
  select(NET, term, t_str_sig)   

tdist_net_stats <- neteucldist_lm %>% 
  filter(NET != "Total") %>%
  ungroup %>% group_by(term) %>%
  mutate(p_FDR = p.adjust(p.value, method = "fdr")) %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered")) %>%
  mutate(t_str = report_tstat(statistic, df_residual, p.value),
         sig_mark = if_else(p_FDR <= 0.05, "*", ""),
         t_str_sig = if_else(p.value < 0.10, str_c(t_str, sig_mark), "-")) 

neteucldist_lm_report<- tdist_net_stats %>%
  select(NET, term, t_str_sig) %>%
  bind_rows(neteucldist_lm_totalline) %>%
  spread(term, t_str_sig) %>%
  parse_NET_for_table() %>%
  arrange(NET)

# write.csv(neteucldist_lm_report, "../plots/tdist_effects_table.csv", row.names = F)
knitr::kable(neteucldist_lm_report, caption = "Effect of age and ASD diagnosis of personalized vertex locations.*Significant after false discovery rate correction across 6 networks. Additional model covariates: Site, Sex, Full IQ, total cortical surface area, top two principal components of fMRI scan quality. Residual degrees of freedom = 886.",
      col.names = c("Network", "Age Effect", "Diagnosis Effect", "Age by Diagnisis Interaction"))
      
```
## we started to see that Leuven and 




```{r}
tdist_model_DXxAge <- function(df) {
  lm(Mean_mm ~ DX_GROUP*Age_Centered + SEX + FIQ_Centered + TotalSurf_Centered +  site + Comp.1 + Comp.2, data = df)
}

## calculate total model statistics (included the Residual SS)
mnet_tdist_Seimens <- mnet_tdist %>%
  filter(ScanMan == "Siemens")

neteucldist_glance <- mnet_tdist_Seimens %>%
  filter(NET == "Total") %>%
  do(glance(tdist_model_DXxAge(.)))

## run the full model under 30
neteucldist_lm <- mnet_tdist_Seimens %>%
  filter(AGE_AT_SCAN < 31) %>%
  group_by(NET) %>%
  do(tidy(tdist_model_DXxAge(.))) %>%
  ungroup() %>%
  mutate(df_residual = neteucldist_glance$df.residual[1])

## print the Network results, FDR corrected for the Age Effect
neteucldist_lm_totalline <- neteucldist_lm %>%
  filter(NET == "Total") %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered")) %>%
  mutate(t_str_sig = report_tstat(statistic, df_residual, p.value)) %>%
  select(NET, term, t_str_sig)   

tdist_net_stats <- neteucldist_lm %>% 
  filter(NET != "Total") %>%
  ungroup %>% group_by(term) %>%
  mutate(p_FDR = p.adjust(p.value, method = "fdr")) %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered")) %>%
  mutate(t_str = report_tstat(statistic, df_residual, p.value),
         sig_mark = if_else(p_FDR <= 0.05, "*", ""),
         t_str_sig = if_else(p.value < 0.10, str_c(t_str, sig_mark), "-")) 

neteucldist_lm_report<- tdist_net_stats %>%
  select(NET, term, t_str_sig) %>%
  bind_rows(neteucldist_lm_totalline) %>%
  spread(term, t_str_sig) %>%
  parse_NET_for_table() %>%
  arrange(NET)

# write.csv(neteucldist_lm_report, "../plots/tdist_effects_table.csv", row.names = F)
knitr::kable(neteucldist_lm_report, caption = "Effect of age and ASD diagnosis of personalized vertex locations.*Significant after false discovery rate correction across 6 networks. Additional model covariates: Site, Sex, Full IQ, total cortical surface area, top two principal components of fMRI scan quality. Residual degrees of freedom = 886.",
      col.names = c("Network", "Age Effect", "Diagnosis Effect", "Age by Diagnisis Interaction"))
      
```

```{r}
tdist_model_DXxAge <- function(df) {
  lm(Mean_mm ~ DX_GROUP*Age_Centered + SEX + FIQ_Centered + TotalSurf_Centered +  site + log10(mean_fd) + dvars, data = df)
}

## calculate total model statistics (included the Residual SS)
neteucldist_glance <- mnet_tdist %>%
  filter(NET == "Total") %>% 
  do(glance(tdist_model_DXxAge(.)))

## run the full model under 30
neteucldist_lm <- mnet_tdist %>%
  filter(AGE_AT_SCAN < 31) %>%
  group_by(NET) %>%
  do(tidy(tdist_model_DXxAge(.))) %>%
  ungroup() %>%
  mutate(df_residual = neteucldist_glance$df.residual[1])

## print the Network results, FDR corrected for the Age Effect
neteucldist_lm_totalline <- neteucldist_lm %>%
  filter(NET == "Total") %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered", "log10(mean_fd)", "dvars")) %>%
  mutate(t_str_sig = report_tstat(statistic, df_residual, p.value)) %>%
  select(NET, term, t_str_sig)   

tdist_net_stats <- neteucldist_lm %>% 
  filter(NET != "Total") %>%
  ungroup %>% group_by(term) %>%
  mutate(p_FDR = p.adjust(p.value, method = "fdr")) %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered", "log10(mean_fd)", "dvars")) %>%
  mutate(t_str = report_tstat(statistic, df_residual, p.value),
         sig_mark = if_else(p_FDR <= 0.05, "*", ""),
         t_str_sig = if_else(p.value < 0.10, str_c(t_str, sig_mark), "-")) 

neteucldist_lm_report<- tdist_net_stats %>%
  select(NET, term, t_str_sig) %>%
  bind_rows(neteucldist_lm_totalline) %>%
  parse_NET_for_table() %>%
  arrange(term, NET)

# write.csv(neteucldist_lm_report, "../plots/tdist_effects_table.csv", row.names = F)
knitr::kable(neteucldist_lm_report, caption = "Effect of age and ASD diagnosis of personalized vertex locations.*Significant after false discovery rate correction across 6 networks. Additional model covariates: Site, Sex, Full IQ, total cortical surface area, mean FD and DVARS. Residual degrees of freedom = 886.", col.names = c("Network", "term", "t statistic"))
```

#### plot mean fd vs total


```{r}
mnet_tdist %>%
  filter(NET == "Total") %>%
  ungroup() %>%
  mutate(motion_type = if_else((mean_fd < 0.1 & num_fd == 0), "low", "high")) %>%
  group_by(motion_type) %>%
  summarise(n = n(),
            Mean = mean(Mean_mm),
            SD = sd(Mean_mm))
```
```{r}
neteucldist_lm_split_motion <- mnet_tdist %>%
  filter(AGE_AT_SCAN < 31, NET == "Total",
         !(site %in% c("Leuven", "Trinity"))) %>%
  mutate(motion_type = if_else((mean_fd < 0.1 & num_fd == 0), "low", "high")) %>%
  group_by(motion_type) %>%
  do(tidy(tdist_model_DXxAge(.))) %>%
  ungroup() %>%
  mutate(df_residual = neteucldist_glance$df.residual[1])

neteucldist_lm_split_motion %>%
  filter(term %in% c("DX_GROUPTD", "Age_Centered", "DX_GROUPTD:Age_Centered", "log10(mean_fd)", "dvars")) %>%
  select(motion_type, term, statistic, p.value) %>%
  knitr::kable()
```
```{r fig.width = 5, fig.height= 4}

  
ggplot(filter(mnet_tdist, NET == "Total"), 
       aes(x = mean_fd, y = Mean_mm, color = ScanMan)) +
  geom_point(aes(color = ScanMan), alpha = 0.5) +
  geom_smooth(method = "lm") +
  scale_x_log10(breaks = seq(0, 0.4, 0.05)) +
#  geom_text(aes(x = 0.1, y = 10, label = filter(neteucldist_lm_report,
#                           term == "log10(mean_fd)", 
#                           NET == "Total")$t_str_sig)) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "Mean Framewise Displacement (mm)", y = "Mean distance (mm) from template vertex",
       color = NULL)
```





```{r fig.width = 5, fig.height= 4}
ggplot(filter(mnet_tdist, NET == "Total"), 
       aes(x = dvars, y = Mean_mm, color = ScanMan)) +
  geom_point(alpha = 0.5) +
  geom_smooth(method = "lm") +
#  scale_x_log10(breaks = seq(0, 0.4, 0.05)) +
#  geom_text(aes(x = 0.1, y = 10, label = filter(neteucldist_lm_report,
#                           term == "log10(mean_fd)", 
#                           NET == "Total")$t_str_sig)) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "DVARS", y = "Mean distance (mm) from template vertex",
       color = NULL)
```


```{r fig.width = 5, fig.height= 4}
ggplot(filter(mnet_tdist, NET == "Total"), 
       aes(x = Comp.1, y = Mean_mm, color = ScanMan)) +
  geom_point(alpha = 0.5) +
  geom_smooth(method = "lm") +
#  scale_x_log10(breaks = seq(0, 0.4, 0.05)) +
#  geom_text(aes(x = 0.1, y = 10, label = filter(neteucldist_lm_report,
#                           term == "log10(mean_fd)", 
#                           NET == "Total")$t_str_sig)) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "Comp.1", y = "Mean distance (mm) from template vertex",
       color = NULL)
```
```{r fig.width = 5, fig.height= 4}
ggplot(filter(mnet_tdist, NET == "Total"), 
       aes(x = Comp.2, y = Mean_mm, color = ScanMan)) +
  geom_point(alpha = 0.5) +
  geom_smooth(method = "lm") +
#  scale_x_log10(breaks = seq(0, 0.4, 0.05)) +
#  geom_text(aes(x = 0.1, y = 10, label = filter(neteucldist_lm_report,
#                           term == "log10(mean_fd)", 
#                           NET == "Total")$t_str_sig)) +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "Comp.2", y = "Mean distance (mm) from template vertex",
       color = NULL)
```

```{r}
tdist_site_resid <- mnet_tdist %>%
  ungroup() %>%
  filter(NET == "Total") %>%
  do(mod = lm(Mean_mm ~ site, .), original = (.)) %>%
  augment(mod, data = original)

fd_cors <- tdist_site_resid %>%
  gather(resid_yes, mm, Mean_mm, .resid) %>%
  mutate(resid_lab = recode(resid_yes, 
                            "Mean_mm"="Raw Distance", 
                            ".resid" = "Site Centered")) %>%
  group_by(resid_yes) %>%
  summarise(rho = cor.test(mm, log10(mean_fd))$estimate,
            pval = cor.test(mm, log10(mean_fd))$p.value,
            r_str = sprintf("r=%3.2f, p=%s",rho, report_pvalue(pval)),
            max_mm = max(mm),
            min_fd = min(mean_fd))

plt_fd <- tdist_site_resid %>%
  gather(resid_yes, mm, Mean_mm, .resid) %>%
  mutate(resid_lab = recode(resid_yes, 
                            "Mean_mm"="Raw Distance", 
                            ".resid" = "Site Centered")) %>%
  ggplot(aes(x = mean_fd, y = mm)) +
  geom_point(aes(color = site), alpha = 0.5) +
#  geom_text(aes(label = r_str, x= min_fd, y = max_mm), data = fd_cors) +
  geom_smooth(method = "lm", color = "black") +
  scale_x_log10(breaks = seq(0, 0.4, 0.05)) +
#  geom_text(aes(x = 0.1, y = 10, label = filter(neteucldist_lm_report,
#                           term == "log10(mean_fd)", 
#                           NET == "Total")$t_str_sig)) +
  facet_wrap(~resid_lab, scales = "free") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "Mean Framewise Displacement (mm)", y = "Mean distance (mm) from template vertex",
       color = NULL)

plt_fd
```
```{r}
dvars_cors <- tdist_site_resid %>%
  gather(resid_yes, mm, Mean_mm, .resid) %>%
  mutate(resid_lab = recode(resid_yes, 
                            "Mean_mm"="Raw Distance", 
                            ".resid" = "Site Centered")) %>%
  ungroup %>% group_by(resid_yes) %>%
  summarise(rho = cor.test(mm, dvars)$estimate,
            pval = cor.test(mm, dvars)$p.value) %>%
  mutate(r_str = sprintf("r=%3.2f, p=%s",rho, report_pvalue(pval)))

plt_dvars <- tdist_site_resid %>%
  gather(resid_yes, mm, Mean_mm, .resid) %>%
  mutate(resid_lab = recode(resid_yes, 
                            "Mean_mm"="Raw Distance", 
                            ".resid" = "Site Centered")) %>%
  ggplot(aes(x = dvars, y = mm)) +
  geom_point(aes(color = site), alpha = 0.5) +
  geom_smooth(method = "lm", color = "black") +
  facet_wrap(~resid_lab, scales = "free") +
  theme_bw() +
  theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
  labs(x = "DVARS", y = "Mean distance (mm) from template vertex",
       color = NULL) 
plt_dvars
```



```{r fig.height = 7, fig.width=7}
library(cowplot)

plt2 <- plot_grid(plt_fd, plt_dvars, ncol = 1)

ggsave(plt2, filename = here('plots','suppl_figure_motion.png'),
       height = 7, width = 7)
```

```{r}
fd_cors
```

```{r}
knitr::kable(dvars_cors)
```



