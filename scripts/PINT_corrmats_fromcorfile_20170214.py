
# coding: utf-8

# In[5]:

import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import glob
import os
sns.set(context="paper", font="monospace")
get_ipython().magic(u'matplotlib inline')


# In[9]:

def make_heatmap(corrmat_file, output_png, summary_df):
    
    corrmat = pd.read_csv(corrmat_file)
    # Set up the matplotlib figure
    f, ax = plt.subplots(figsize=(10, 8))

    # Draw the heatmap using seaborn
    sns.heatmap(corrmat, vmax=.8, square=True)

    # Use matplotlib directly to emphasize known networks
    for i in summary_df.index:
        if i and summary_df.loc[i,'NETWORK'] != summary_df.loc[i-1,'NETWORK']:
            ax.axhline(len(summary_df) - i, c="w",linewidth=3.0 )
            ax.axvline(i, c="w",linewidth=3.0)
    f.tight_layout()
    f.savefig(output_png)


# In[10]:

summary_df = pd.read_csv('../phenotypic_and_qc/Yeo7_80verts_201610.csv')
make_heatmap('../plots/mean_ivertex_corrmat.csv', '../plots/mean_ivertex_corrmat.png', summary_df)
make_heatmap('../plots/mean_tvertex_corrmat.csv', '../plots/mean_tvertex_corrmat.png', summary_df)


# In[ ]:



