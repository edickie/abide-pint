for dir in `ls -1d roi*U31`;
  do
  wb_command -cifti-math '(z * ((x+y)>0))' \
   $dir/ActualDifference_Significant.dscalar.nii \
   -var x $dir/Significant_U_percentile.dscalar.nii \
   -var y $dir/Significant_L_percentile.dscalar.nii \
   -var z $dir/ActualDifference.dscalar.nii
  done

## 42 - 60
mergelst=""
for roi in `seq 43 60`; do
  mergelst="${mergelst} -cifti roi${roi}_DXcodes_U31/ActualDifference_Significant.dscalar.nii"
  done
wb_command -cifti-merge SA_Sigificant_DXdiff_U31_2D.dscalar.nii $mergelst

wb_command -cifti-reduce SA_Sigificant_DXdiff_U31_2D.dscalar.nii SUM SA_Sigificant_DXdiff_U31_1D.dscalar.nii
