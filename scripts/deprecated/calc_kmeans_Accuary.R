library(dplry)
## function I annoyingly had to figure out to find the most frequent label
## Note: it returns the mode as a string
mymode <- function(x) {
  result <- names(sort(table(x), decreasing = T)[1])
  return(result)
}

## runs kmeans we 6 clusters on the output and returns a vector of Accuracy for each item
kmeans_Acc <- function(basedf,ts_file) {
  vertex_ts <- read.csv(file = ts_file, header=FALSE)
  basedf$vertex.kmeans <- kmeans(vertex_ts, 6)$cluster
  basedf <- basedf %>%
    group_by(NETWORK) %>%
    mutate(NETk = as.numeric(mymode(vertex.kmeans)))
  basedf$kNetAcc[basedf$vertex.kmeans == basedf$NETk] <- 1
  basedf$kNetAcc[basedf$vertex.kmeans != basedf$NETk] <- 0
  Accuracy <- basedf$kNetAcc
  return(Accuracy)
}

Phenotypic_V1_0b <- read.csv("/projects/edickie/analysis/ABIDE/phenotypic/Phenotypic_V1_0b.csv")
pint_outputdir <- "/projects/edickie/analysis/abide_PINT/20160610_PINT_Yeo7-76v_rad4-8-12"
setwd(pint_outputdir)

## get a list of all participants outputs by recursively searching for "sum
summary_files <- list.files(pint_outputdir, pattern = "summary.csv", recursive = TRUE)
templatedf <- read.csv(summary_files[1])
obasedf <- templatedf

## loop through all meants files and output a kmeans Accuracy value for each roi for each participant
for (summaryfile in summary_files) {
  subid <- dirname(summaryfile)
  for (vert_type in c('tvertex','ivertex')) {
    ts_file <- gsub("summary.csv",paste0(vert_type,'_meants.csv'),summaryfile)
    ts_Acc <- kmeans_Acc(templatedf, ts_file)
    obasedf <- cbind(obasedf, ts_Acc)
    names(obasedf)[names(obasedf)== "ts_Acc"] <- paste(subid,vert_type,"KmeansAcc",sep='_')
  }
}

all_tvertex_Acc <- obasedf %>%
                    select(ends_with("ivertex_KmeansAcc"))

templatedf$mean_tvertex_Acc <- apply(all_tvertex_Acc,1,mean)

arrange(templatedf, NETWORK, mean_tvertex_Acc)
write.csv(templatedf, 'meanKmeanstvertexAcc_byroiidx.csv', row.names = F)
