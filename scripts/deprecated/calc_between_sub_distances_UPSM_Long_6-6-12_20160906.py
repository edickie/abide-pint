
# coding: utf-8

# In[1]:

from epitome.docopt import docopt
from scipy import stats, linalg
import epitome as epi
import numpy as np
import nibabel as nib
import random
import os
import sys
import tempfile
import shutil
import subprocess
import pandas as pd
import nibabel.gifti.giftiio


# In[2]:

#mkdir a tmpdir for the
tmpdir = tempfile.mkdtemp()


# In[32]:

DEBUG=False
DRYRUN=False
def docmd(cmdlist):
    "sends a command (inputed as a list) to the shell"
    if DEBUG: print ' '.join(cmdlist)
    if not DRYRUN: subprocess.call(cmdlist)


# In[33]:

## measuring distance
def get_surf_distances(surf, orig_vertex, radius_search, tmpdir=tmpdir):
    '''
    uses wb_command -surface-geodesic-distance command to measure
    distance between two vertices on the surface
    '''
    surf_distance = os.path.join(tmpdir, "distancecalc.shape.gii")
    docmd(['wb_command', '-surface-geodesic-distance',
            surf, str(orig_vertex), surf_distance,
            '-limit', str(radius_search)])
    distances = epi.utilities.load_gii_data(surf_distance)
    return(distances)


# In[36]:

surfL='../cvs_avg35_inMNI152/MNINonLinear/fsaverage_LR32k/cvs_avg35_inMNI152.L.midthickness.32k_fs_LR.surf.gii'
surfR='../cvs_avg35_inMNI152/MNINonLinear/fsaverage_LR32k/cvs_avg35_inMNI152.R.midthickness.32k_fs_LR.surf.gii'
allvertices_csv = '../20160906_PINT_UPSM_Long_Yeo7-80v_rad6-6-12/20160906_PINT_UPSM_Long_Yeo7-80v_rad6-6-12_concatenatedresults.csv'
output_csv = '../20160906_PINT_UPSM_Long_Yeo7-80v_rad6-6-12/20160906_PINT_UPSM_Long_Yeo7-80v_rad6-6-12_sub2sub_distances.csv'

## read in the thing
vertices_df = pd.read_csv(allvertices_csv)

## get the ivertex columns
ivertex_columns = [x for x in vertices_df.columns if "ivertex" in x]
radius_search = 100


# In[5]:

## set up my fancy long table
thing1 = ['subid1', 'subid2']
thing2 = vertices_df['roiidx'].tolist()
thing1.extend(thing2)
result = pd.DataFrame(columns = thing1)
idx = 0
for i in ivertex_columns[:-1]:
    for j in ivertex_columns[1:]:
        result.append(pd.DataFrame(columns = thing1, index = [idx]))
        result.loc[idx,'subid1'] = i
        result.loc[idx,'subid2'] = j
        idx += 1


# In[34]:

## loop over all vertices
for vidx in vertices_df['roiidx'].tolist():

    ## determine the roiidx and the hemisphere (surface)
    roiidx = vertices_df.loc[vidx,'roiidx']
    if vertices_df.loc[vidx,'hemi'] is 'L': surf = surfL
    if vertices_df.loc[vidx,'hemi'] is 'R': surf = surfR

    ## loop over all pairs of PINT result outputs
    for idx in result.index:

        ## read the vertex numbers
        ivertexcol_x = result.loc[idx,'subid1']
        ivertexcol_y = result.loc[idx,'subid2']
        vertex_x = int(vertices_df.loc[vertices_df['roiidx']==roiidx,ivertexcol_x])
        vertex_y= int(vertices_df.loc[vertices_df['roiidx']==roiidx,ivertexcol_y])

        ## if this is the first one - or if vertex_x has changed - load the distances
        if idx == 0:
            distances = get_surf_distances(surf, vertex_x, radius_search)
        elif ivertexcol_x != result.loc[idx - 1,'subid1']:
            distances = get_surf_distances(surf, vertex_x, radius_search)

        ## if vertex_x is equal to vertex_y set to 0
        ## otherwise read the distance from the vertex column
        if vertex_x == vertex_y:
            result.iloc[idx,roiidx] = 0
        else:
            result.iloc[idx,roiidx] = distances[vertex_y,0]




# In[37]:

### write out the resutls to a csv
result.to_csv(output_csv,columns = thing1, index = False)


# In[33]:




# In[ ]:
