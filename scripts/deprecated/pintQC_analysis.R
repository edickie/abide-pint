## we need dplyr and ggplot2
library(randomForest)
library("dplyr")
library("ggplot2")
library("tidyr")

# this is a little function to grab the numeric subid out of the site+numeric 
getsubid <- function(fullsubid) {
  strsplit(fullsubid,'_')[[1]][2]
}

## quick way to repeat a column
rep.col<-function(x,n){
  matrix(rep(x,each=n), ncol=n, byrow=TRUE)
}

## load the qc and get the numeric subject id ("subject")
qc_PINT.6.6.12 <- read.csv("~/Documents/ABIDE/PINT/scripts/qc_PINT-6-6-12.csv", header=FALSE)
names(qc_PINT.6.6.12) <- c("subid","pintQC","pintQCnote")

## get the numeric subid
qc_PINT.6.6.12$subject <- NA
for (i in 1:nrow(qc_PINT.6.6.12)) {
  qc_PINT.6.6.12$subject[i] <- getsubid(as.character(qc_PINT.6.6.12$subid[i]))
}

## load the QAP measures spreadsheet
QAP <- read.csv("~/Documents/ABIDE/PINT/scripts/abide_func_QAP_plusmymeasures.csv", sep="")

## calculate the top 3 principle components of the QAP measures
QAPmeasures <- QAP %>% 
                select(starts_with("func")) %>% 
                filter(!is.na(func_fwhm)) 
sQAPmeasures <- data.frame(apply(QAPmeasures,2,scale))
fit <- princomp(sQAPmeasures, cor=TRUE)
summary(fit) # print variance accounted for
#loadings(fit) # pc loadings
#plot(fit,type="lines") # scree plot
fit$scores # the principal components
#biplot(fit)
QAP <- cbind(filter(QAP,!is.na(func_fwhm)),fit$scores[, 1:3])

## bin ages into three groups (children, adolesents, adults)
ggplot(QAP, aes(x = age, color = dx)) + geom_density()
QAP$agegroup <- NA
QAP$agegroup[QAP$age < 12] <- "< 12"
QAP$agegroup[12 <= QAP$age & QAP$age < 18] <- "12-18"
QAP$agegroup[18 <= QAP$age ] <- "> 18"
QAP$agegroup <- factor(QAP$agegroup)

## merge both together
qc_df <- merge(qc_PINT.6.6.12,QAP, by = "subject")

## look at counts by subgroup
ftable(qc_df$site,qc_df$pintQC)
chisq.test(ftable(qc_df$dx,qc_df$pintQC)) ## unfortunately it does appear that there are more fails in the autism group
ftable(qc_df$agegroup, qc_df$ dx, qc_df$pintQC)

## plots of QC fails by original data type
ggplot(qc_df, aes(x = site, y = Comp.1, color = pintQC)) + geom_jitter() + facet_grid(~dx)
ggplot(qc_df, aes(x = site, y = Comp.2, color = pintQC)) + geom_jitter() + facet_grid(~dx)
ggplot(qc_df, aes(x = site, y = Comp.3, color = pintQC)) + geom_jitter() + facet_grid(~dx)

## run a random forest to predict wether or not the subject would fail by the QC information...
## We get an accuracy of 80%


# in this cross validation example, we use the iris data set to 
# predict the Sepal Length from the other variables in the dataset 
# with the random forest model 

yvar = "pintQC"
xvars = c("site", grep("^func",names(qc_df),value=T))
idvar = "subid"
data <- qc_df
k = 10 #Folds
numP = 100
result_predicted <- data[ ,c(idvar,yvar)]
result_importance <- as.data.frame(cbind("perm"=rep(1:numP, each=k), 
                                   "fold"=rep(1:k,numP),
                                   matrix(nrow=k*numP, 
                                   ncol=length(xvars),
                                   dimnames=list(1:(k*numP),c(xvars)))))

for (p in 1:numP) {
  # sample from 1 to k, nrow times (the number of observations in the data)
  data$id <- sample(1:k, nrow(data), replace = TRUE)
  list <- 1:k
  
  # prediction and testset data frames that we add to with each iteration over
  # the folds
  
  prediction <- data.frame()
  testsetCopy <- data.frame()
  
  for (i in 1:k) {
    # remove rows with id i from dataframe to create training set
    # select rows with id i to create test set
    trainingset <- subset(data, id %in% list[-i])
    testset <- subset(data, id %in% c(i))
    
    # run a random forest model
    mymodel <- randomForest(x=trainingset[ ,xvars],y=trainingset[ ,yvar], ntree = 500)
    
    # remove response column 1, Sepal.Length
    temp <- as.data.frame(predict(mymodel, testset[,xvars]))
    # append this iteration's predictions to the end of the prediction data frame
    prediction <- rbind(prediction, temp)
    
    #importance <- cbind(importance,mymodel$importance)
    result_importance[result_importance$fold==i & result_importance$perm == p,
                      3:ncol(result_importance)] <- mymodel$importance
    
    # append this iteration's test set to the test set copy data frame
    # keep only the Sepal Length Column
    testsetCopy <- rbind(testsetCopy, as.data.frame(testset[,idvar]))
    
    print(paste("Done Fold",i))
  }
  
  # add predictions and actual Sepal Length values
  thisresult <- cbind(prediction, testsetCopy)
  names(thisresult) <- c(paste0("Predicted_",p),idvar)
  result_predicted <- merge(result_predicted,thisresult, by=idvar)
}

## melt the Predictors data frame to make the plot
sorted_Predictors <- names(sort(apply(result_importance[3:ncol(result_importance)],2,mean)))
gimportance <- result_importance %>% gather(Predictor,MeanDecreaseGini,-fold,-perm)
gimportance$Predictor<- factor(gimportance$Predictor, levels=sorted_Predictors)
 
## the pretty plot
ggplot(gimportance, aes(x=Predictor,y=MeanDecreaseGini)) + 
  geom_boxplot() +
  coord_flip()

## look at the cools Predictions
rpasses <- select(result_predicted,starts_with("Predicted")) == "pass"
result_predicted$PropPass <- apply(rpasses,1,mean)
result_predicted$QCPass50 <- ifelse(result_predicted$PropPass > 0.5, "pass", "fail")

# tseq = seq(0.01,0.99,0.01)
# result_stats <- data.frame("threshold" = tseq,
#                            "TPR" = numeric(length(tseq)),
#                            "FNR" = numeric(length(tseq)))
# for (i in 1:nrow(result_stats)){
#   thrPredicted <- ifelse(result_predicted$PropPass > result_stats$threshold[i], "pass", "fail")
#   correct <- result_predicted[ ,yvar]==thrPredicted
#   passes <- result_predicted[ ,yvar]=="pass"
#   fails <- result_predicted[ ,yvar]=="fail"
#   result_stats$TPR[i] <- sum(correct[passes])/sum(passes)
#   result_stats$FNR[i] <- 1 - sum(correct[fails])/sum(fails)
# }
# 
# ggplot(result_stats, aes(y = TPR, x = FNR)) +
#   geom_line() +
#   geom_abline (intercept = 0, slope = 1) +
#   theme_bw()
### calc hits and false alarms
qc_df <- merge(qc_df, select(result_predicted, subid, PropPass, QCPass50), by ="subid")
qc_df$qc_cat <- NA
qc_df$qc_cat[qc_df$pintQC=="pass" & qc_df$QCPass50=="pass"] <- "TruePass"
qc_df$qc_cat[qc_df$pintQC=="pass" & qc_df$QCPass50=="fail"] <- "Miss"
qc_df$qc_cat[qc_df$pintQC=="fail" & qc_df$QCPass50=="pass"] <- "QAPpass"
qc_df$qc_cat[qc_df$pintQC=="fail" & qc_df$QCPass50=="fail"] <- "TrueFail"
qc_df$qc_cat <- factor(qc_df$qc_cat)
summary(qc_df$qc_cat)
ggplot(qc_df, aes(x = site, y = Comp.1, color = qc_cat)) +
  geom_jitter() +
  facet_grid(agegroup~dx) +
  coord_flip()

# As an example use Mean Absolute Error as Evalution 
ftable(qc_df$site,qc_df$qc_cat)
ftable(qc_df$dx, qc_df$qc_cat)
chisq.test(ftable(qc_df$dx, qc_df$qc_cat))
ftable(qc_df$dx, qc_df$agegroup, qc_df$qc_cat)
chisq.test(ftable(qc_df$agegroup, qc_df$qc_cat))
ftable(qc_df$site, qc_df$dx, qc_df$agegroup, qc_df$QCPass50)
detach(package:plyr)
sitebyage_pass <-qc_df %>% 
  group_by(agegroup, site) %>%
  summarise(count=sum(QCPass50=="pass")) %>%
  filter(count > 5)

sitebyage_fails <-qc_df %>% 
  group_by(agegroup, site) %>%
  summarise(count=sum(QCPass50=="pass")) %>%
  filter(count < 5)  

## make new columns for the age groups
qc_df$QCPassU12 <- 0
qc_df$QCPassU12[ qc_df$agegroup == "< 12" & qc_df$QCPass50 == "pass"] <- 1
for (SITE in sitebyage_fails$site[sitebyage$agegroup=="< 12"]) {
  qc_df$QCPassU12[ qc_df$site == SITE ] <- 0
}

qc_df$QCPassADO <- 0
qc_df$QCPassADO[ qc_df$agegroup == "12-18" & qc_df$QCPass50 == "pass"] <- 1
for (SITE in sitebyage_fails$site[sitebyage$agegroup=="12-18"]) {
  qc_df$QCPassADO[ qc_df$site == SITE ] <- 0
}

qc_df$QCPassO18 <- 0
qc_df$QCPassO18[ qc_df$agegroup == "> 18" & qc_df$QCPass50 == "pass"] <- 1
# note we are not dropping UM_2 because it can probably be pooled with UM_1

sitebyage_pass <-qc_df %>% 
  group_by(agegroup, site) %>%
  summarise(count=sum(pintQC=="pass")) %>%
  filter(count > 5)

sitebyage_fails <-qc_df %>% 
  group_by(agegroup, site) %>%
  summarise(count=sum(pintQC=="pass")) %>%
  filter(count < 5)  

## make new columns for the age groups
qc_df$pintQCU12 <- 0
qc_df$pintQCU12[ qc_df$agegroup == "< 12" & qc_df$pintQC == "pass"] <- 1
for (SITE in sitebyage_fails$site[sitebyage$agegroup=="< 12"]) {
  qc_df$pintQCU12[ qc_df$site == SITE ] <- 0
}

qc_df$pintQCADO <- 0
qc_df$pintQCADO[ qc_df$agegroup == "12-18" & qc_df$pintQC == "pass"] <- 1
for (SITE in sitebyage_fails$site[sitebyage$agegroup=="12-18"]) {
  qc_df$pintQCADO[ qc_df$site == SITE ] <- 0
}

qc_df$pintQCO18 <- 0
qc_df$pintQCO18[ qc_df$agegroup == "> 18" & qc_df$pintQC == "pass"] <- 1
# note we are not dropping UM_2 because it can probably be pooled with UM_1

## wrote out the data at this point to a csv
#write.csv(qc_df, "~/Documents/abide_PINT/scripts/qced_sublists_20160814.csv", row.names=F)


# 
# ##########################################
# ## Now let's actually compare what happended to teh PINT results
# PINT6results <- read.csv("~/Documents/ABIDE/PINT/6-6-12-results/20160610_PINT_Yeo7-76v_rad6-6-12_concatenatedresults.csv")
# distances <- data.frame(t(select(PINT6results, ends_with("distance"))))
# names(distances) <- as.character(1:76)
# distances$subject <- NA
# for (i in 1:nrow(distances)) {
#   distances$subject[i] <- getsubid(row.names(distances)[i])
# }
# 
# distg <- gather(distances, key = roi, value = distance, -subject)
# distg$roiidx <- as.numeric(distg$roi)
# distgm <- merge(select(PINT6results,roiidx,NETWORK),distg, by="roiidx")
# distances_plus <- merge(qc_df, distgm, by="subject")
# distances_plus <- distances_plus %>% filter(pintQC=="pass")
# 
# distances_plus <- filter(distances_plus, distance < 40)
# 
# 
# ggplot(filter(distances_plus, distance < 40), aes(x=agegroup, y=distance, color=factor(dx))) +
#   geom_boxplot() +
#   facet_wrap(~roiidx)
# 
# 
# ####### Run a random forest to detect dx...
# distances_long <- merge(qc_df, distances, by="subject")
# distances_long <- distances_long %>% filter(pintQC=="pass")
# 
# names(distances_long)[35:110] <- paste0("dist_",names(distances_long[35:110]))
# #randomhelpme <- distances_long[ ,c(7,6,9:11, 31:33, 35:110)]
# randomhelpme <- distances_long[ ,c(7,6,9:11, 31:33)]
# randomhelpme$dx <- factor(randomhelpme$dx)
# data <- randomhelpme
# # in this cross validation example, we use the iris data set to 
# # predict the Sepal Length from the other variables in the dataset 
# # with the random forest model 
# 
# k = 10 #Folds
# 
# # sample from 1 to k, nrow times (the number of observations in the data)
# data$id <- sample(1:k, nrow(data), replace = TRUE)
# list <- 1:k
# 
# # prediction and testset data frames that we add to with each iteration over
# # the folds
# 
# prediction <- data.frame()
# testsetCopy <- data.frame()
# importance <- data.frame()
# 
# 
# for (i in 1:k){
#   # remove rows with id i from dataframe to create training set
#   # select rows with id i to create test set
#   trainingset <- subset(data, id %in% list[-i])
#   testset <- subset(data, id %in% c(i))
#   
#   # run a random forest model
#   mymodel <- randomForest(trainingset$dx ~ ., data = trainingset, ntree = 100)
#   
#   # remove response column 1, Sepal.Length
#   temp <- as.data.frame(predict(mymodel, testset[,-1]))
#   # append this iteration's predictions to the end of the prediction data frame
#   prediction <- rbind(prediction, temp)
#   
#   #importance <- cbind(importance,mymodel$importance)
#   
#   # append this iteration's test set to the test set copy data frame
#   # keep only the Sepal Length Column
#   testsetCopy <- rbind(testsetCopy, as.data.frame(testset[,1]))
#   
#   print(paste("Done Fold",i))
# }
# 
# # add predictions and actual Sepal Length values
# result <- cbind(prediction, testsetCopy[, 1])
# names(result) <- c("Predicted", "Actual")
# result$Correct <- result$Actual == result$Predicted
# summary(result)
