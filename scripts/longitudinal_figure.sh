##
source ~/ciftify_env.sh

Pitt_outputs=/home/edickie/code/abide-PINT/data/20161101_PINT_fixn_UPSM_Long_Yeo7-80v_rad6-6-12

subs="50007 50026"

surfL=~/Documents/abide_PINT/HCP_S900_GroupAvg_v1/S900.L.midthickness_MSMAll.32k_fs_LR.surf.gii
surfR=~/Documents/abide_PINT/HCP_S900_GroupAvg_v1/S900.R.midthickness_MSMAll.32k_fs_LR.surf.gii

outputdir=~/Documents/long_figure
mkdir -p ${outputdir}


sub="50007"
ciftify-surface-rois \
  --vertex-col 'tvertex'\
  ${Pitt_outputs}/${sub}_baseline/50007_baseline_Yeo7-80v_rad6-6-12_summary.csv \
  6 \
  ${surfL} ${surfR} \
  ${outputdir}/tvertex_6mm.dscalar.nii

for sub in ${subs}; do
  for timepoint in "baseline" "followup_1"; do
    ciftify-surface-rois \
      --vertex-col 'ivertex'\
      ${Pitt_outputs}/${sub}_${timepoint}/${sub}_${timepoint}_Yeo7-80v_rad6-6-12_summary.csv \
      6 \
      ${surfL} ${surfR} \
      ${outputdir}/${sub}_${timepoint}_ivertex_6mm.dscalar.nii
  done
done


surfL=~/Documents/abide_PINT/HCP_S900_GroupAvg_v1/S900.L.very_inflated_MSMAll.32k_fs_LR.surf.gii
surfR=~/Documents/abide_PINT/HCP_S900_GroupAvg_v1/S900.R.very_inflated_MSMAll.32k_fs_LR.surf.gii

sub="50007"
ciftify-surface-rois \
  --vertex-col 'tvertex'\
  ${Pitt_outputs}/${sub}_baseline/50007_baseline_Yeo7-80v_rad6-6-12_summary.csv \
  2 \
  ${surfL} ${surfR} \
  ${outputdir}/tvertex_2mm.dscalar.nii

for sub in ${subs}; do
  for timepoint in "baseline" "followup_1"; do
    ciftify-surface-rois \
      --vertex-col 'ivertex'\
      ${Pitt_outputs}/${sub}_${timepoint}/${sub}_${timepoint}_Yeo7-80v_rad6-6-12_summary.csv \
      2 \
      ${surfL} ${surfR} \
      ${outputdir}/${sub}_${timepoint}_ivertex_2mm.dscalar.nii
  done
done

sub="50007"
ciftify-surface-rois \
  --vertex-col 'tvertex'\
  ${Pitt_outputs}/${sub}_baseline/50007_baseline_Yeo7-80v_rad6-6-12_summary.csv \
  3 \
  ${surfL} ${surfR} \
  ${outputdir}/tvertex_3mm.dscalar.nii

for sub in ${subs}; do
  for timepoint in "baseline" "followup_1"; do
    ciftify-surface-rois \
      --vertex-col 'ivertex'\
      ${Pitt_outputs}/${sub}_${timepoint}/${sub}_${timepoint}_Yeo7-80v_rad6-6-12_summary.csv \
      3 \
      ${surfL} ${surfR} \
      ${outputdir}/${sub}_${timepoint}_ivertex_3mm.dscalar.nii
  done
done

ciftify-surface-rois \
  --vertex-col 'tvertex'\
  --labels-col 'NETWORK' \
  ${Pitt_outputs}/${sub}_baseline/${sub}_baseline_Yeo7-80v_rad6-6-12_summary.csv \
  2 \
  ${surfL} ${surfR} \
  ${outputdir}/tvertex_2mm.dscalar.nii

for sub in ${subs}; do
  for timepoint in "baseline" "followup_1"; do
    ciftify-surface-rois \
      --vertex-col 'ivertex'\
      --labels-col 'NETWORK' \
      ${Pitt_outputs}/${sub}_${timepoint}/${sub}_${timepoint}_Yeo7-80v_rad6-6-12_summary.csv \
      2 \
      ${surfL} ${surfR} \
      ${outputdir}/${sub}_${timepoint}_ivertex_2mm.dscalar.nii
  done
done

wb_command -cifti-label-import \
  tvertex_2mm.dscalar.nii \
  RSN-labels_whiteEX.txt \
  tvertex_2mm.dlabel.nii

wb_command -cifti-label-import \
  50026_followup_1_ivertex_2mm.dscalar.nii \
  RSN-labels_yellow1EX.txt \
  50026_followup_1_ivertex_2mm.dlabel.nii

wb_command -cifti-label-import \
  50026_baseline_ivertex_2mm.dscalar.nii \
  RSN-labels_yellow2EX.txt \
  50026_baseline_ivertex_2mm.dlabel.nii

wb_command -cifti-label-import \
  50007_baseline_ivertex_2mm.dscalar.nii \
  RSN-labels_dlilacEX.txt \
  50007_baseline_ivertex_2mm.dlabel.nii

wb_command -cifti-label-import \
  50007_followup_1_ivertex_2mm.dscalar.nii \
  RSN-labels_lilacEX.txt \
  50007_followup_1_ivertex_2mm.dlabel.nii
