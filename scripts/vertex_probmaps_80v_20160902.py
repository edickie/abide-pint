
# coding: utf-8

# In[13]:

import matplotlib.pyplot as plt
import subprocess
import pandas as pd
import glob
import os
import tempfile
import shutil

# In[22]:

outdir='../20160902_PINT_Yeo7-80v_rad6-6-12-results/'
vertexresults = '../20160902_PINT_Yeo7-80v_rad6-6-12-results/20160902_PINT_Yeo7-80v_rad6-6-12_concatenatedresults.csv'
subjectdf = pd.read_csv('qced_sublists_20160814.csv')
hcp_data_dir = '../'
subject = 'cvs_avg35_inMNI152'
sampling_rad = 6
search_rad = 6
padding_rad = 12
summarydf = pd.read_csv('Yeo7_80verts_201608.csv')
DEBUG = False
DRYRUN = False
tmpdir = tempfile.mkdtemp()

# In[14]:

def docmd(cmdlist):
    "sends a command (inputed as a list) to the shell"
    if DEBUG: print ' '.join(cmdlist)
    if not DRYRUN: subprocess.call(cmdlist)


# In[15]:

def make_probmaps(vertexdf, sublist, listname, hcp_data_dir, subject, sampling_rad, tmpdir, outdir):

    allrois_filelist = []
    collist = [s + '_ivertex' for s in sublist]
    for roiidx in vertexdf.loc[:,'roiidx'].get_values():

        groupvertices = vertexdf.loc[vertexdf.loc[:,'roiidx']==roiidx,collist].transpose()
        groupvertices['hemi'] = vertexdf.loc[vertexdf.loc[:,'roiidx']==roiidx,'hemi'].get_values()[0]
        groupvertices.columns = ['vertex', 'hemi']
        groupvertices.to_csv(os.path.join(tmpdir,'vertices.csv'),index = False)

        ## make the overlaying ROIs
        roidscalar = os.path.join(tmpdir,'roi{}.dscalar.nii'.format(roiidx))
        docmd(['epi-surface-rois',
                '--probmap',
                os.path.join(tmpdir,'vertices.csv'),
                str(sampling_rad),
                os.path.join(hcp_data_dir, subject, 'MNINonLinear','fsaverage_LR32k',
                    '{}.L.midthickness.32k_fs_LR.surf.gii'.format(subject)),
                os.path.join(hcp_data_dir, subject, 'MNINonLinear','fsaverage_LR32k',
                    '{}.R.midthickness.32k_fs_LR.surf.gii'.format(subject)),
                roidscalar])

        allrois_filelist.extend(['-cifti',roidscalar])

    ## merge all the subs
    cmdlist = ['wb_command','-cifti-merge',
           os.path.join(outdir,'vertex_proi_{}.dscalar.nii'.format(listname))]
    cmdlist.extend(allrois_filelist)
    docmd(cmdlist)

    ## merge all the subs by network into one map
    for network in vertexdf.loc[:,'NETWORK'].unique():
        netcmd = ['wb_command','-cifti-merge',
                  os.path.join(tmpdir, 'network{}_rois2D.dscalar.nii'.format(network))]
        for roiidx in vertexdf.loc[vertexdf.loc[:,'NETWORK']==network,'roiidx']:
            netcmd.extend(['-cifti', os.path.join(tmpdir,'roi{}.dscalar.nii'.format(roiidx))])
        docmd(netcmd)
        docmd(['wb_command',
               '-cifti-reduce',
              os.path.join(tmpdir, 'network{}_rois2D.dscalar.nii'.format(network)),
              'SUM',
              os.path.join(tmpdir, 'network{}_rois1D.dscalar.nii'.format(network))])

    ## combine the network maps into one output
    netmerge_cmd = ['wb_command','-cifti-merge',
                    os.path.join(outdir, 'network_proi_{}.dscalar.nii'.format(listname))]
    for network in vertexdf.loc[:,'NETWORK'].unique():
        netmerge_cmd.extend(['-cifti',
                             os.path.join(tmpdir, 'network{}_rois1D.dscalar.nii'.format(network))])
    docmd(netmerge_cmd)






# In[24]:

## read in the vertex results
vertexdf = pd.read_csv(vertexresults)


# In[25]:

# for dx in [1,2]:
#     for sitename in subjectdf.loc[:,'siteid'].unique():

#         ## slice the subject list dataframe to find the subject list
#         if dx is 1 : listname = '{}ASD'.format(sitename)
#         if dx is 2 : listname = '{}TD'.format(sitename)
#         subdf = subjectdf.loc[subjectdf.loc[:,'siteid']== sitename,:]
#         sublist = subdf.loc[subjectdf.loc[:,'dx']==dx,'subid'].tolist()

#         ## make the probmaps for each vertex for each group..
#         print(listname)
#         make_probmaps(vertexdf, sublist, listname, hcp_data_dir, subject, sampling_rad, tmpdir, outdir)

subjectdf.loc[subjectdf.loc[:,'QCPass50']=="pass" ,:]



# In[26]:

for dx in [1,2]:

    ## slice the subject list dataframe to find the subject list
    if dx is 1 : listname = 'allASDQAPpass'
    if dx is 2 : listname = 'allTDQAPpass'
    subdf = subjectdf.loc[subjectdf.loc[:,'QCPass50']=="pass" ,:]
    sublist = subdf.loc[subjectdf.loc[:,'dx']==dx,'subid'].tolist()

    ## make the probmaps for each vertex for each group..
    print(listname)
    make_probmaps(vertexdf, sublist, listname, hcp_data_dir, subject, sampling_rad, tmpdir, outdir)


# In[27]:



listname = 'allQAPpass'
subdf = subjectdf.loc[subjectdf.loc[:,'QCPass50']=="pass" ,:]
sublist = subdf.loc[:,'subid'].tolist()

## make the probmaps for each vertex for each group..
print(listname)
make_probmaps(vertexdf, sublist, listname, hcp_data_dir, subject, sampling_rad, tmpdir, outdir)


# In[28]:

## within the 3 age groups separatly
for qcgroup in ['QCPassU12', 'QCPassADO', 'QCPassO18']:
    for dx in [1,2]:

        ## slice the subject list dataframe to find the subject list
        if dx is 1 : listname = 'allASD{}'.format(qcgroup)
        if dx is 2 : listname = 'allTD{}'.format(qcgroup)
        subdf = subjectdf.loc[subjectdf.loc[:,qcgroup]==1 ,:]
        sublist = subdf.loc[subjectdf.loc[:,'dx']==dx,'subid'].tolist()

        ## make the probmaps for each vertex for each group..
        print(listname)
        make_probmaps(vertexdf, sublist, listname, hcp_data_dir, subject, sampling_rad, tmpdir, outdir)


# In[29]:

## make diff maps for each age group
for qcgroup in ['QCPassU12', 'QCPassADO', 'QCPassO18', 'QAPpass']:
    docmd(['wb_command',
           '-cifti-math',
           '(x - y)',
           os.path.join(outdir, 'network_proi_dxdiff_all{}.dscalar.nii'.format(qcgroup)),
           '-var', 'x',
           os.path.join(outdir, 'network_proi_allTD{}.dscalar.nii'.format(qcgroup)),
           '-var','y',
           os.path.join(outdir, 'network_proi_allASD{}.dscalar.nii'.format(qcgroup))])


# In[ ]:
